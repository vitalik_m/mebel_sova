### mebel-sova

if you get error `Missing required parameters for [Route: voyager.bread.delete] [URI: admin/bread/{id}]` after click on "Tools"->"Database" or "Tools"->"Menu Builder"->"Menu Builder" see [this topic](https://github.com/the-control-group/voyager/issues/4586)

## deploy

* copy example.env to .env and set actual values to variables
* run `composer install`
* run `php artisan voyager:install`
* run `php artisan migrate`
* run `php artisan db:seed`