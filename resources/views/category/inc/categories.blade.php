@foreach($categories as $cat)
    <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 similar-goods__itemsimilar">
        <figure class="similar-goods__item ">
            <a href="{{ categoryPath($cat) }}">
                <img src="{{ $cat->image !== null ? asset('storage/' . $cat->image) : '/storage/templates/no-image.png' }}"
                    class="img-responsive"
                    alt="{{ $cat->image_alt }}"
                    title="{{ $cat->image_title }}">
            </a>
        </figure>
        <h3 class="similar-goods__itemheader"><a href="{{ categoryPath($cat) }}">{{ $cat->title }}</a></h3>
        <a href="{{ categoryPath($cat) }}" class="similar-goods__readmore">Посмотреть</a>
    </div>

    {{ $categories->links() }}
@endforeach