@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'catalog.category', $category) }}
@endsection

@section('content')
    <!-- BEGIN block_center -->
    <section class="similar-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                {{ Breadcrumbs::render('catalog.category', $category) }}
                </div>
            </div>

            <div class="row similar-goods">
                @if(isset($filtersList) && $filtersList->isNotEmpty())
                <div class="col-sm-3">
                    @include('filters.filters')
                </div>
                @endif

                <div class="@if(isset($filtersList) && $filtersList->isNotEmpty())col-sm-9 @else col-sm-12 @endif">
                    <h1 class="similar-goods__header">{{ $category->title }}</h1>

                    @includeWhen($children->isNotEmpty() && !$products->isNotEmpty(), 'category.inc.categories', ['categories' => $children])

                @foreach($products as $product)
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 similar-goods__itemsimilar">
                    <figure class="similar-goods__item ">
                        <a href="{{ productPath($product, $category) }}">
                            <img src="{{ asset('storage/' . $product->main_image) }}"
                                class="img-responsive"
                                alt="{{ $product->title }}"
                                title="{{ $product->title }}">
                        </a>
                    </figure>

                    <h3 class="similar-goods__itemheader"><a href="{{ productPath($product, $category) }}">{{ $product->title }}</a></h3>
                    <a href="{{ productPath($product, $category) }}" class="similar-goods__readmore">Посмотреть</a>
                </div>
                @endforeach

                    {{ $products->links() }}

            </div>
        </div>

    </section>
    <!-- END block_center -->
@endsection