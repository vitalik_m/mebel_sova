@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'articles.list') }}
@endsection

@section('content')
    <!-- BEGIN block_center -->
    <section class="similar-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    {{ Breadcrumbs::render('articles.list') }}
                </div>
            </div>

            <div class="row similar-goods">
                <h1 class="similar-goods__header">Каталог</h1>

                @include('category.inc.categories', ['categories' => $categories])

            </div>
        </div>

    </section>
    <!-- END block_center -->
@endsection