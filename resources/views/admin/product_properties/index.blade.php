@extends('voyager::master')

@section('content')

    @include('admin.inc.msgs')

    <div class="container-fluid">
        <div class="page-content">
            <h1 class="page-title">Свойста товаров</h1>
            <a href="" class="btn btn-success btn-add-new" id="addProperty">
                <i class="voyager-plus"></i> <span>Добавить свойство товара</span>
            </a>
        </div>

        <div class="page-content">
            <form action="{{ route('admin.products.property.store') }}" method="post" style="visibility: hidden" id="addingForm" class="addingForm">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                <input type="text" name="title" id="addingPropertyTitle" placeholder="Введите название нового свойства" required>
                <input type="text" name="unit" id="addingPropertyUnit" placeholder="Введите Единицы измерения">

                <select name="value_type" id="addingPropertyValueType">
                    <option value="" selected="selected">Выберите тип значения</option>
                    <option value="float">Цифровой</option>
                    <option value="char">Текстовый</option>
                    <option value="special">Специальный</option>
{{--                    <option value="bool">{{$value->valueName->name}}</option>--}}
                </select>

                <button class="btn btn-success btn-add-new"> Добавить </button>
            </form>
        </div>

        <div class="page-content">
            @if( !empty($properties) )
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">№</th>
                            <th scope="col">Название</th>
                            <th scope="col">Единицы измерения</th>
                            <th scope="col">Тип</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($properties as $property)
                        <tr>
                            <td>{{ $property['id'] }}</td>
                            <td>
                                <input type="text" name="title" value="{{ $property['title'] }}" form="update-form-{{ $property['id'] }}" required/>
                            </td>
                            <td>
                                <input type="text" name="unit" value="{{ $property['unit'] }}" form="update-form-{{ $property['id'] }}" />
                            </td>

                            <td>{{ $property['value_type'] }}</td>
                            <td>{{ $property['slug'] }}</td>

                            <td class="no-sort no-click" id="bread-actions" width="25%">
                                <form id="update-form-{{ $property['id'] }}" action="{{ route('admin.products.property.update', ['id' => $property->id]) }}" method="post" style="display: inline-block">
                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    <input type="hidden" name="_method" value="PUT"/>

                                    <button class="btn btn-sm btn-warning pull-right delete">
                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Изменить</span>
                                    </button>
                                </form>

                                @if($property['value_type'] == 'char')
                                <a href="{{ route('admin.products.property.value.char.names', ['property' => $property['id']]) }}"
                                   title="Значения" class="btn btn-sm btn-primary pull-left edit" style="display: inline-block">
                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Значения</span>
                                </a>
                                @endif

                                <form action="{{ route('admin.products.property.delete', ['id' => $property['id']]) }}" method="post" style="display: inline-block">
                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    <input type="hidden" name="_method" value="delete" />

                                    <button class="btn btn-sm btn-danger pull-left delete">
                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Удалить</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                Нет свойств товаров. <a href="">Добавить свойство товара</a>
            @endif
        </div>
    </div>

    <script>
        var operationsValue = {
            init: function(){
                document.getElementById('addProperty').addEventListener('click', function(e){
                    e.preventDefault();
                    operationsValue.showAddingForm();
                });

                operationsValue.bindShowUpdateBtns();
                operationsValue.bindCancelUpdateBtns();
            },
            showAddingForm: function() {
                document.getElementById('addingForm').setAttribute("style", "visibility: visible");
            },

            showUpdateForm: function(e){
                document.querySelector('#'+e.currentTarget.dataset.for+' .edit').setAttribute("style", "display: block");
            },
            hideUpdateForm: function(e){
                document.querySelector('#'+e.currentTarget.dataset.for+' .edit').setAttribute("style", "display: none");
            },

            bindShowUpdateBtns() {
                let btns = document.getElementsByClassName('show-update-btn');

                [].forEach.call(btns, function (el) {
                    el.addEventListener('click', function(e){
                        operationsValue.showUpdateForm(e);
                    });
                });
            },
            bindCancelUpdateBtns() {
                let btns = document.getElementsByClassName('cancel-update-btn');

                [].forEach.call(btns, function (el) {
                    el.addEventListener('click', function(e){
                        operationsValue.hideUpdateForm(e);
                    });
                });
            }
        };

        operationsValue.init()
    </script>
@stop