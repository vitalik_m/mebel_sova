@extends('voyager::master')

@section('content')

    @include('admin.inc.msgs')

    <div class="container-fluid">
        <div class="page-content">
            <h1 class="page-title">Значения свойста @if(!empty($property))"{{ $property->title }}"@endif</h1>
            <a href="" class="btn btn-success btn-add-new" id="addValue">
                <i class="voyager-plus"></i> <span>Добавить значение @if(!empty($property)) для "{{ $property->title }}"@endif</span>
            </a>
        </div>

        <div class="page-content">
            <form action="{{ route('admin.products.property.value.char.name.store') }}" method="post" style="visibility: hidden" id="addingForm" class="addingForm">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                <input type="text" name="valueName" id="valueName" placeholder="Введите название нового значения" required>
                <input type="hidden" name="property_id" id="property_id" value="{{ $property->id }}">

                <button class="btn btn-success btn-add-new"> Добавить </button>
            </form>
        </div>

        <div class="page-content">
            @if( !empty($values) )
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">№</th>
                            <th scope="col">Название значения</th>
                            @if(empty($property))
                                <th scope="col">Свойство</th>
                            @endif
                            <th scope="col">Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($values as $value)
                        <tr id="value-name-{{ $value->id }}">
                            <td>{{ $value->id }}</td>

                            <td class="name">
                                <span class="text">{{ $value->name }}</span>

                                <div class="edit" style="display: none">
                                    <form action="{{ route('admin.products.property.value.char.name.update', ['id' => $value->id]) }}" method="post" style="display: inline-block">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input type="hidden" name="_method" value="PUT"/>

                                        <input type="text" name="name"/>

                                        <button class="btn btn-sm btn-warning warning">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Изменить</span>
                                        </button>
                                    </form>

                                    <button class="btn btn-sm btn-success success cancel-update-btn" data-for="value-name-{{ $value->id }}">
                                        <i class="voyager-x"></i> <span class="hidden-xs hidden-sm">Отмена</span>
                                    </button>
                                </div>
                            </td>

                            @if(empty($property))
                                <td>{{ $value->property->title }}</td>
                            @endif

                            <td class="no-sort no-click" id="bread-actions" width="25%">
                                <button class="btn btn-sm btn-warning pull-right warning show-update-btn" data-for="value-name-{{ $value->id }}">
                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Изменить</span>
                                </button>

                                <form action="{{ route('admin.products.property.value.char.name.delete', ['valueName_id' => $value->id]) }}" method="post">
                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    <input type="hidden" name="_method" value="delete" />

                                    <button class="btn btn-sm btn-danger pull-right delete">
                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Удалить</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                Нет значений @if(!empty($property))свойства "{{ $property->title }}"@endif. <a href="">Добавить значение @if(!empty($property))для "{{ $property->title }}"@endif</a>
            @endif
        </div>
    </div>

    <script>
        var operationsValue = {
            init: function(){
                document.getElementById('addValue').addEventListener('click', function(e){
                    e.preventDefault();
                    operationsValue.showAddingForm();
                });

                operationsValue.bindShowUpdateBtns();
                operationsValue.bindCancelUpdateBtns();
            },
            showAddingForm: function() {
                document.getElementById('addingForm').setAttribute("style", "visibility: visible");
            },

            showUpdateForm: function(e){
                document.querySelector('#'+e.currentTarget.dataset.for+' .edit').setAttribute("style", "display: block");
            },
            hideUpdateForm: function(e){
                document.querySelector('#'+e.currentTarget.dataset.for+' .edit').setAttribute("style", "display: none");
            },

            bindShowUpdateBtns() {
                let btns = document.getElementsByClassName('show-update-btn');

                [].forEach.call(btns, function (el) {
                    el.addEventListener('click', function(e){
                        operationsValue.showUpdateForm(e);
                    });
                });
            },
            bindCancelUpdateBtns() {
                let btns = document.getElementsByClassName('cancel-update-btn');

                [].forEach.call(btns, function (el) {
                    el.addEventListener('click', function(e){
                        operationsValue.hideUpdateForm(e);
                    });
                });
            }
        };

        operationsValue.init()
    </script>
@stop