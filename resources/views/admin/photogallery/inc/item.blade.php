<div class="form-group image" data-index="{{ $i }}" {{ $isNew ?? ''}}>
    <div class="image-block">
        @if(!empty($number))
            <div class="btn-above-item-img arrow-btn-left">
                <span class="arrow"></span>
            </div>
            <input type="number" pattern="^[ 0-9]+$" class="btn-above-item-img index_number" value="{{ $number }}">
            <div class="btn-above-item-img arrow-btn-right">
                <span class="arrow"></span>
            </div>
        @endif

        <label class="file_upload_group">
            <span class="upload_file_label">Добавить / изменить изображение</span><br>

            <img src="{{ !empty($image) ? Storage::disk('public')->url($image) : '/storage/templates/no-image.png'}}" id="preview_{!! $i!!}"
                 height="200"
                 alt="Загружаемый файл">
            <br>
            <input type="hidden" class="gallery_upload_old_img" name="gallery_upload_{!! $i!!}_old_img"
                    value="{{ !empty($image) ? $image : ''}}"/>
            <input type="file" name="gallery_upload_{!! $i!!}" class="form-control-file"
                   onchange="photogallery.functs.previewFile(this)"/>
        </label>

        <span id="clear_upload_img_{!! $i!!}" onclick="photogallery.deleteItem(this)" class="clear_upload_img">+</span>
    </div>



    <div class="properties_block">
        <h3>Свойства</h3>

        <input type="text" class="gallery_upload_alt" name="gallery_upload_{!! $i!!}_alt" placeholder="Alt"
               @if( isset($gallery) && is_numeric($i) &&  $gallery['alts'][$i] )
               value="{{ $gallery['alts'][$i] }}"
               @endif
               class="form-control"/>
        <input type="text" class="gallery_upload_title" name="gallery_upload_{!! $i!!}_title" placeholder="Title"
               @if( isset($gallery) && is_numeric($i) &&  $gallery['titles'][$i] )
               value="{{ $gallery['titles'][$i] }}"
               @endif
               class="form-control"/>
    </div>



    <div class="displaying box_{!! $i!!} view_type_block">
        <h3>Показать</h3>

        <label>
            <input type="radio" class="as-gallery" name="gallery_upload_{!! $i!!}_display" value="1"
                @if( (isset($gallery) && is_numeric($i) &&  $gallery['display_as'][$i] == 1) || !is_numeric($i) )
                    checked="checked"
                @endif
            />
            <span>В слайдере</span>
        </label>
        <label>
            <input type="radio" class="as-subproduct" name="gallery_upload_{!! $i!!}_display" value="2"
                @if( isset($gallery) && is_numeric($i) &&  $gallery['display_as'][$i] == 2 )
                    checked="checked"
                @endif
            />
            <span>Как субтовар</span>
        </label>

        <div class="fields_for_subproducts"
            @if(isset($gallery) && is_numeric($i) &&  $gallery['display_as'][$i] == 2)
                style="display:block"
            @endif
        >
            <input type="text" name="title_for_{!! $i!!}_sub_0" placeholder="Заголовок"
            @if( isset($gallery) && is_numeric($i) && $gallery['fields_for_subproduct'][$i]['title'] )
                value="{{ $gallery['fields_for_subproduct'][($i)]['title'] }}"
            @endif
            />

            <input type="text" name="key_for_{!! $i!!}_sub_0" placeholder="Параметр"
            @if( isset($gallery) && is_numeric($i) && $gallery['fields_for_subproduct'][$i]['key'] )
                value="{{ $gallery['fields_for_subproduct'][($i)]['key'] }}"
            @endif
            />

            <input type="text" name="value_for_{!! $i!!}_sub_0" placeholder="Значение"
            @if( isset($gallery) && is_numeric($i) && $gallery['fields_for_subproduct'][$i]['value'] )
                value="{{ $gallery['fields_for_subproduct'][($i)]['value'] }}"
            @endif
            />
        </div>
    </div>
</div>