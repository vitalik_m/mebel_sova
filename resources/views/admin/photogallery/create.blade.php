@extends('voyager::master')
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">

            <div class="page-content col-sm-12">
                <h1 class="page-title">Добавление новой галереи</h1>
            </div>

            <div class="page-content col-sm-12">
                <form id="store_photogallery" method="post" enctype="multipart/form-data" action="{{ route('admin.photogallery.store') }}" class="container-fluid">
                    @csrf

                    <div class="row">
                        <div class="form-group col-sm-2">
                            <label>
                                <span>Название</span>
                                <input type="text" name="title" required placeholder="Название" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label>
                                <span>Код для вставки</span>
                                <input type="text" name="shortcode" readonly data-slug-origin="title"
                                       placeholder="Код для вставки" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label>
                                <input type="checkbox" name="enable" checked="checked" class="toggleswitch"/>
                                <span>Включить</span>
                            </label>
                        </div>
                    </div>

                    <div class="row items-block">
                        @include('admin.photogallery.inc.item', ['i' => '0', 'number' => 1])
                    </div>

                    <div class="row btns-block">
                        <div class="form-group col-sm-12" id="gallery_add_button">
                            <a id="add_new_file_upload_group" class="btn btn-success btn-add-new">+ Добавить поле</a>
                        </div>

                        <div class="form-group col-sm-12" id="gallery_save_button">
                            <button type="submit" form="store_photogallery" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>

                @section('javascript')
                    <script src="{{ asset('js/photogallery.js') }}"></script>

                    <script>
                        var photogallery_data = {
                            itemTemp: function() {
                                return `@include('admin.photogallery.inc.item', [
                                    'i' => '` + photogallery.get.countPhotos() + `',
                                    'image' => null,
                                    'number' => '` + (1+photogallery.get.countPhotos()) + `',
                                    'isNew' => 'data-isNew=true'])`;
                            },
                            photogalleryId: null,
                            isEditing: false,

                            loader: `@include('layouts.inc.loader')`
                        }
                    </script>
                @endsection
            </div>

        </div>
    </div>
@stop