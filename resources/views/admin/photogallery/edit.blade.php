@extends('voyager::master')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="page-content col-sm-12">
                <h1 class="page-title">Редактировать галерею</h1>
            </div>

            <div class="page-content">
                <form method="post" action="{{ route('admin.photogallery.update', ['photogallery' => $gallery->id]) }}" enctype="multipart/form-data" class="container-fluid">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="form-group col-sm-2">
                            <label>
                                <span>Название</span>
                                <input type="text" name="title" value="{{ $gallery->title }}" required
                                       placeholder="Название" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-md-2">
                            <label>
                                <span>Код для вставки</span>
                                <input type="text" name="shortcode" readonly data-slug-origin="title"
                                       placeholder="Код для вставки" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-md-2">
                            <label>
                                <input type="checkbox" name="enable"
                                       {{ ($gallery->enabled ? 'checked="checked"' : '') }} class="toggleswitch"/>
                                <span>Включить</span>
                            </label>
                        </div>

                        <div>
                            <input type="hidden" name="deleted_images"/>
                        </div>
                    </div>

                    <div class="row items-block">
                        @php $number=1; @endphp
                    @forelse ($gallery['images'] as $key => $image)
                        @include('admin.photogallery.inc.item', ['i' => $key, 'number' => $number])
                        @php $number++ @endphp
                    @empty
                        @include('admin.photogallery.inc.item', ['i' => '0'])
                    @endforelse
                    </div>

                    <div class="form-group" id="gallery_add_button">
                        <a id="add_new_file_upload_group" class="btn btn-success btn-add-new">+ Добавить поле</a>
                    </div>

                    <div class="form-group" id="gallery_save_button">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>

                @section('javascript')
                <script src="{{ asset('js/photogallery.js?v') }}"></script>

                <script>
                    var photogallery_data = {
                        itemTemp: function() {
                            return `@include('admin.photogallery.inc.item', ['i' => '` + photogallery.get.countPhotos() + `', 'image' => null, 'number' => '` + (1+photogallery.get.countPhotos()) + `', 'isNew' => 'data-isNew=true'])`;
                        },
                        photogalleryId: {{ $gallery->id }},
                        delItemLink: '{{ route('admin.photogallery.item.delete', ['photogallery' => $gallery->id,'id' => 'item_id']) }}',

                        shortcode: '{{ $gallery['shortcode'] }}',
                        isEditing: true,
                        msgs: {
                            deleteFail: 'Ошибка удаления'
                        },

                        loader: `@include('layouts.inc.loader')`
                    }
                </script>
            @endsection
            </div>

        </div>
    </div>
@stop