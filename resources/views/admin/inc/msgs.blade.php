@if($errors->all())
<div class="alerts">
    @foreach ($errors->all() as $error)
        <div class="alert alert-warning alert-name-warning">
            {{ $error }}
        </div>
    @endforeach
</div>
@endif