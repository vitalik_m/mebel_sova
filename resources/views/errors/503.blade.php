@extends('errors::minimal')

@if(config('app.env') == 'production')
    @section('title', __('errors.Service Unavailable'))
    @section('code', '503')
    @section('message', __($exception->getMessage() ?: 'errors.Service Unavailable'))
@else
    <div class="flex-center position-ref full-height">
        <div class="code">Ой-ой </div>

        <div class="message" style="padding: 10px;">
            Сайт переехал на <a href="http://1728533.iz386382.web.hosting-test.net/">новый  хостинг</a> в случае неисправностей пишите в вайбер чат :)</div>
    </div>
@endif