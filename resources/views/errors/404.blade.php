@extends('errors::minimal')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('/assets/css/style.min.css') }}">
</head>

<body>
    <header></header>
    <div class="error-section">
        <div class="error-section__container">
            <h1 class="error-section__header">404</h1>
            <h2 class="error-section__itemheader">Страница не найдена</h2>
            <a href="{{ URL::previous() }}" class="error-section__btn error-section__btn_transparent">Вернуться назад</a>
            &nbsp;
            <a href="/" class="error-section__btn">Перейти на главную</a></div>
    </div>
    <footer></footer>
    <script src="{{ url('/assets/js/main.min.js') }}" defer="defer"></script>
</body>

</html>
{{--
@section('title', __('Not Found'))
@section('code', '404')
@section('message', __('Not Found'))
--}}
