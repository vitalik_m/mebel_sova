<html>
    <head>
        <title>Письмо с формы на странице задать вопрос</title>
    </head>
    <body>
        <p>Имя: {{ $form['name'] }}</p>
        <p>Телефон: {{ $form['phone'] }}</p>
        <p>E-mail: {{ $form['email'] }}</p>
        <p>Город: {{ $form['city'] }}</p>
        <p>Вопрос: {{ $form['question'] }}</p>
    </body>
</html>