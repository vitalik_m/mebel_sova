@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'question') }}
@endsection

@section('content')
    <section class="similar-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                {{ Breadcrumbs::render('question') }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="question">
                    <h1 class="question__title">Задать вопрос</h1>

                    <div class="messages">
                        @foreach ($errors->all() as $message)
                            <div class="error">{{ $message }}</div>
                        @endforeach

                        @if(!empty($success))
                            <div class="success">Сообщение отправлено</div>
                        @endif
                    </div>

                    <div class="question__form">
                        <form action="{{ route('question.send') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @csrf

                            <div class="question__row">
                                <label class="question__label" for="name">* Имя</label>
                                <input class="question__input" placeholder="Как к вам обращаться?"
                                       name="name" id="name" value="{{ old('name') }}" required>
                            </div>
                            <div class="question__row">
                                <label class="question__label" for="phone">Телефон</label>
                                <input class="question__input" placeholder="Ваш телефон" type="tel"
                                       name="phone" id="phone" value="{{ old('phone') }}">
                            </div>
                            <div class="question__row">
                                <label class="question__label" for="email">Email</label>
                                <input class="question__input" placeholder="mebel@sova.com.ua" type="email"
                                       name="email" id="email" value="{{ old('email') }}">
                            </div>
                            <div class="question__row">
                                <label class="question__label" for="city">Город</label>
                                <input class="question__input" placeholder="В каком городе вы находитесь?"
                                       name="city" id="city" value="{{ old('city') }}">
                            </div>
                            <div class="question__row">
                                <label class="question__label" for="question">* Ваш вопрос</label>
                                <textarea placeholder="Ваш вопрос" class="question__textarea"
                                      name="question" id="question" required>{{ old('question') }}</textarea>
                            </div>

                            <div class="question__row">
                                <label class="question__label"></label>
                                <div class="code">
                                    <div class="captcha">{!! captcha_img() !!}</div>
{{--                                    <a class="refresh" href="#" onclick="document.getElementById('qimg').src='/img.php?x='+Math.random();return false;"></a>--}}
                                </div>
                            </div>
                            <div class="question__row">
                                <label class="question__label" for="captcha">* Введите код</label>
                                <input class="question__input" placeholder="Что написано на картинке"
                                       name="captcha" id="captcha" required>
                            </div>

                            <button class="question__btn" type="submit">
                                Отправить
                            </button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@endsection