@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'articles.view', $article) }}
@endsection

@section('content')
    <section class="similar-section">
<div class="container">
    <div class="row">
        <div class="col-12">
            {{ Breadcrumbs::render('articles.view', $article) }}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="about-company">

                @include('articles.inc.back')

                <h1 class="about-company__title">{{ $article->title }}</h1>

                <div class="about-company__text">
                    {!! $article->content !!}
                </div>

                @include('articles.inc.back')

            </div>
        </div>
    </div>
</div>
    </section>

@endsection