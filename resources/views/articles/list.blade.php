@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'articles.list') }}
@endsection

@section('content')
    <section class="similar-section">
<div class="container">
    <div class="row">
        <div class="col-12">
            {{ Breadcrumbs::render('articles.list') }}
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="about-company">
                <h1 class="about-company__title">СТАТЬИ</h1>

                <div class="search-page__text">
                    @foreach($articles as $article)

                    <div class="article">
                        <p>
                            <a href="{{ route('articles.view', ['article' => $article->slug]) }}">{{ $article->title }}</a>
                            <br/>
                            {!! $article->intro !!}
                        </p>
                    </div>

                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
    </section>

@endsection