<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-31012952-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <meta charset="utf-8">

    <link rel="canonical" href="{{ rtrim(env('APP_URL'),'/').request()->getPathInfo() }}" />

    @php
        $default = setting('seo.title');
        switch (true) {
            case isset($page) :
                if( !empty($page->seo_title) ) {
                    $default = $page->seo_title;
                } else {
                    $default = $page->title;
                }
            break;
            case isset($category) :
                if( !empty($category->seo_title) ) {
                    $default = $category->seo_title;
                } else {
                    $default = $category->title;
                }
            break;
            case isset($product) :
                if( !empty($product->seo_title) ) {
                    $default = $product->seo_title;
                } else {
                    $default = $product->title;
                }
            break;
        }
        if( !is_null(request()->get('page')) ) {
            $default .= ' - страница '.request()->get('page');
        }
    @endphp

    <title>@yield('title', $default)</title>
    <meta name="description" content="@yield('meta_description')">
    <meta name="title" content="@yield('meta_title')">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1" name="viewport">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          crossorigin="anonymous" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" rel="stylesheet"
          crossorigin="anonymous" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp">

    <link href="/css/custom.css" rel="stylesheet">

    <script>!function (e) {
            "use strict";

            function t(t, n) {
                return e.localStorage && localStorage[t + "_content"] && localStorage[t + "_file"] === n
            }

            function n(n, o) {
                if (e.localStorage && e.XMLHttpRequest) t(n, o) ? a(localStorage[n + "_content"]) : function (e, t) {
                    var n = new XMLHttpRequest;
                    n.open("GET", t, !0), n.onreadystatechange = function () {
                        4 === n.readyState && 200 === n.status && (a(n.responseText), localStorage[e + "_content"] = n.responseText, localStorage[e + "_file"] = t)
                    }, n.send()
                }(n, o); else {
                    var r = l.createElement("link");
                    r.href = o, r.id = n, r.rel = "stylesheet", r.type = "text/css", l.getElementsByTagName("head")[0].appendChild(r), l.cookie = n
                }
            }

            function a(e) {
                var t = l.createElement("style");
                t.setAttribute("type", "text/css"), l.getElementsByTagName("head")[0].appendChild(t), t.styleSheet ? t.styleSheet.cssText = e : t.innerHTML = e
            }

            var l = e.document;
            e.loadCSS = function (e, t, n) {
                var a, o = l.createElement("link");
                if (t) a = t; else {
                    var r;
                    a = (r = l.querySelectorAll ? l.querySelectorAll("style,link[rel=stylesheet],script") : (l.body || l.getElementsByTagName("head")[0]).childNodes)[r.length - 1]
                }
                var s = l.styleSheets;
                o.rel = "stylesheet", o.href = e, o.media = "only x", a.parentNode.insertBefore(o, t ? a : a.nextSibling);
                var c = function (e) {
                    for (var t = o.href, n = s.length; n--;) if (s[n].href === t) return e();
                    setTimeout(function () {
                        c(e)
                    })
                };
                return o.onloadcssdefined = c, c(function () {
                    o.media = n || "all"
                }), o
            }, e.loadLocalStorageCSS = function (a, o) {
                t(a, o) || l.cookie.indexOf(a) > -1 ? n(a, o) : (s = function () {
                    n(a, o)
                }, (r = e).addEventListener ? r.addEventListener("load", s, !1) : r.attachEvent && r.attachEvent("onload", s));
                var r, s
            }
        }(this)</script>
    <script>loadCSS("/css/main.min.css?ver=1.0.0", !1, "all")</script>

    <noscript>
        <link href="/css/header.min.css" rel="stylesheet">
        <link href="/css/main.min.css" rel="stylesheet">
    </noscript>

    <link href="/css/swiper.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700&subset=cyrillic" rel="stylesheet">

    <link href="/favicons/apple-touch-icon.ico" rel="apple-touch-icon" sizes="180x180">
    <link href="/favicons/favicon-32x32.ico" rel="icon" sizes="32x32" type="image/x-icon">
    <link href="/favicons/favicon-16x16.ico" rel="icon" sizes="16x16" type="image/x-icon">
    <link href="/favicons/manifest.json" rel="manifest">
    <link href="/favicons/safari-pinned-tab.svg" rel="mask-icon" color="#e10d1b">
    <link href="/favicons/favicon.ico" rel="shortcut icon">


    <meta content="#000" name="apple-mobile-web-app-status-bar-style">
    <meta content="#e10d1b" name="msapplication-TileColor">
    <meta content="/favicons/mstile-144x144.ico" name="msapplication-TileImage">
    <meta content="/favicons/browserconfig.xml" name="msapplication-config">
    <meta content="#e10d1b" name="theme-color">

    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Organization",
        "url": "{!! env('APP_URL'); !!}",
        "logo": "{!! env('APP_URL'); !!}img/logo-full.png",
        "contactPoint": [
            {
                "@type": "ContactPoint",
                "telephone": "+38(061)212-58-81",
                "contactType": "customer service",
                "email": "mebelsova.zp@gmail.com"
            },
            {
                "@type": "ContactPoint",
                "telephone": "+38(066)768-69-00",
                "contactType": "customer service"
            }
        ],
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "г. Запорожье, бул. Винтера, 30",
            "addressLocality": "Zaporizhzhya region",
            "addressRegion": "ZP",
            "postalCode": "23101",
            "addressCountry": "UA"
        }
    }
    </script>
    @yield('structureData')

    @yield('header')
</head>

<body>


<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 header__logo"><a href="/" class="logo__link"><img
                            alt="logo" src="/img/logo-full{{ config('app.env') !== 'production' ? '-dev':'' }}.png" class="img-responsive logo-img"></a></div>
            <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 header__search unpadding">
                {!! setting('site.phones_in_header') !!}
                <form action="/search" method="get" class="searchform" id="searchform">
                    <input class="searchform__searchfield" name="word" placeholder="Поиск">
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 header__contacts">{!! setting('site.contacts_in_header') !!}</div>
        </div>
    </div>

    @include('layouts.inc.main_menu')
</header>

@yield('content')

<footer class="owlshop-footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2">
                @include('layouts.inc.footer_menu')
            </div>

            <div class="col-xs-12 col-sm-12 col-md-5 footer__map">
                <a href="https://www.google.com/maps/place/47%C2%B052'11.7%22N+35%C2%B004'13.6%22E/@47.8699061,35.0617012,15z/data=!3m1!4b1!4m13!1m6!3m5!1s0x40dc5e01fe3208ff:0xed8ca86cf8d29181!2z0KHQuNC70YzQv9C-!8m2!3d47.8697865!4d35.070532!3m5!1s0x0:0x0!7e2!8m2!3d47.8699071!4d35.0704561?shorturl=1" target="_blank">
                    <img alt="map" src="/img/map.png">
                </a>
            </div>

            <div class="col-xs-12 col-md-3 col-sm-6">
                {!! setting('site.contacts_in_footer') !!}
            </div>

            <div class="col-xs-12 col-md-2 col-sm-6">
                <img alt="slider-img footer-logo" src="/img/sova-logo.svg"
                     class="img-responsive footer-logo" title="logo">
                <img alt="slider-img manufacturer-logo" src="/img/Logo-Urich-Full.svg"
                     class="img-responsive manufacturer-logo" title="urich">
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="/js/libs.min.js" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="/js/swiper.min.js"></script>
<!-- BEGIN extra_script --><!-- END extra_script -->
<script src="/js/common.js"></script>

<script> (function () {
        const burger = document.querySelector('.burger');
        const menu = burger.nextElementSibling;
        burger.addEventListener('click', () => {
            if (menu.classList.contains('adaptive-nav__active')) {
                menu.classList.remove('adaptive-nav__active');
            } else {
                menu.classList.add('adaptive-nav__active');
            }
        });
    })(); </script>
<script src="/js/lightbox.min.js"></script>
<link href="/css/lightbox.min.css" rel="stylesheet">
<link href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" rel="stylesheet">

<script>
    var galleryTop = new Swiper(".gallery-top", {
        autoHeight: true, //enable auto height
        spaceBetween: 0,
        loop: !0,
        loopedSlides: $(".gallery-top .swiper-wrapper .swiper-slide").length,
        navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"}
    }), galleryThumbs = new Swiper(".gallery-thumbs", {
        spaceBetween: 0,
        loop: !0,
        loopedSlides: $(".gallery-thumbs .swiper-wrapper .swiper-slide").length,
        slidesPerView: "auto",
        touchRatio: .2,
        slideToClickedSlide: !0
    }), loopedSlidesCount = $(".gallery-thumbs .swiper-wrapper .swiper-slide").length;

    if(typeof galleryTop.controller !='undefined') {
        galleryTop.controller.control = galleryThumbs, galleryThumbs.controller.control = galleryTop;
    }
</script>

<script>lightbox.option({resizeDuration: 200, wrapAround: !0})</script>

@yield('scripts')

</body>
</html>
