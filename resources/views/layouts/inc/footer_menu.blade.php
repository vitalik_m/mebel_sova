@php
    $fMenu = menu('main', '_json');
@endphp
<ul class="footer-list footer-list-left">
    @foreach($fMenu as $item)
    <li class="footer-list__item @if($item->url == request()->path()) {{ 'active' }} @endif">
        <a href="{{$item->url}}" class="footer-list__link">{{$item->title}}</a>
    </li>
    @endforeach
</ul>