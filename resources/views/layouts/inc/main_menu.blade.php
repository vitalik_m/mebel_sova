<nav class="main-nav">
    @php
        $menu = \App\ProductsCategory::onlyParents()->get();
    @endphp
    <div class="burger"><i class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></i></div>

    <div class="adaptive-nav">
        <ul class="adaptive-nav__list">
        @foreach($menu as $key => $category)
            <li class="adaptive-nav__item
                    @if($category->child == 'adaptive_menu2_item_active') {{ 'active' }} @endif
                    @if($category->children->isNotEmpty()) {{ 'dropdown' }} @endif ">
                <a href="{{ categoryPath($category) }}" class="main-nav__link">
                    <span>{{ $category->title }}</span>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </a>

                @if($category->children->isNotEmpty())
                    <div class="sub-nav">
                        <ul class="sub-nav__list">
                            @foreach($category->children as $key => $subcategory)
                            <li class="sub-nav__item
                                @if($subcategory->child == 'adaptive_menu2_item_active') {{ 'active' }} @endif ">

                                <a href="{{ categoryPath($subcategory) }}" class="sub-nav__link">
                                    {{$subcategory->title}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </li>
        @endforeach
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="main-nav__list">

                    @if($menu)
                        @foreach($menu as $key => $category)
                        <li class="main-nav__item
                            @if($category->child == 'adaptive_menu2_item_active') {{ 'active' }} @endif
                            @if($category->children->isNotEmpty()) {{ 'dropdown' }} @endif ">

                            <a href="{{ categoryPath($category) }}" class="main-nav__link">
                                <span>{{ $category->title }}</span>
                                <i class="glyphicon glyphicon-chevron-down"></i>
                            </a>

                            @if($category->children->isNotEmpty())
                            <div class="sub-nav">
                                <ul class="sub-nav__list">
                                    @foreach($category->children as $key => $subcategory)
                                    <li class="sub-nav__item
                                        @if($subcategory->child == 'adaptive_menu2_item_active') {{ 'active' }} @endif">

                                        <a href="{{ categoryPath($subcategory) }}" class="sub-nav__link">{{$subcategory->title}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </li>
                        @endforeach
                    @endif
                    
                </ul>
            </div>
        </div>
    </div>
</nav>