@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'pages', $page, 'actions') }}
@endsection

@section('content')
    <section class="similar-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    {{ Breadcrumbs::render('pages', $page, 'actions') }}
                </div>
            </div>

            <div class="row similar-goods">
                <h2 class="similar-goods__header">{{ $page->title ?? 'CПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ' }}</h2>

                @foreach($specialOffers as $product)
                <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 similar-goods__itemsimilar">

                    <figure class="similar-goods__item similar-goods__item_action">
                        <a href="{{ productPath($product) }}">
                            <img src="{{ asset('storage/' . $product->main_image) }}"
                                 class="img-responsive"
                                 alt="{{ $product->title }}"
                                 title="{{ $product->title }}">
                        </a>
                    </figure>

                    <h3 class="similar-goods__itemheader">
                        <a href="{{ productPath($product) }}">{{ $product->title }}</a>
                    </h3>
                    <a href="{{ productPath($product) }}" class="similar-goods__readmore">Посмотреть</a>
                </div>
                @endforeach

            </div>
        </div>

    </section>
@endsection