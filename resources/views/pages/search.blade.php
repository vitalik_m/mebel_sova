@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'search') }}
@endsection

@section('content')
    <section class="similar-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    {{ Breadcrumbs::render('search') }}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="search-page">
                        <h2 class="search-page__title">Результаты поиска «{{ Request::get('word') }}»</h2>
                        <div class="search-page__content">
                            <div class="search-page__text"></div>
                            @if(!is_null($result))
                                @foreach($result as $item)
                                <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 similar-goods__itemsimilar">
                                    <figure class="similar-goods__item {{ $item->special_offer ? 'similar-goods__item_action' : '' }}">
                                        <a href="{{  productPath($item) }}">
                                            <img alt="{{ $item->title }}"
                                                src="{{ asset('storage/' . $item->main_image) }}"
                                                class="img-responsive"
                                                title="{{ $item->title }}">
                                        </a>
                                    </figure>
                                    <h3 class="similar-goods__itemheader"><a href="{{ productPath($item) }}">{{ $item->title }}</a></h3>
                                    <a href="{{ productPath($item) }}" class="similar-goods__readmore">Посмотреть</a>
                                </div>
                                @endforeach
                            @else
                                <div>По вашему запросу ничего не найдено</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection