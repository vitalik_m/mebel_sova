@extends('layouts.main')

@section('header')
    <style>
        hr.contacts{
            margin-top: 40px;
            margin-bottom: 40px;
            border-top: 2px solid #f0f0f0;

        }
    </style>
@endsection

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'pages', $page, 'contacts') }}
@endsection

@section('content')
    <section class="similar-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                {{ Breadcrumbs::render('pages', $page, 'contacts') }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="contact-page">
                    <h1 class="contact-page__title">Контакты</h1>

                    <div class="contact-page__row">
                        <div class="contact-page__left">
                            {!! $page->description !!}
                        </div>
                        <div class="contact-page__right">
                            <img alt="" src="{{ asset('storage/templates/images/deliverys/b_1ea9c2f97cc8e213f9eaff88509a4e30_1334082873.jpg')}}">
                        </div>
                    </div>

{{--                    {DIVIRER}--}}
                </div>
            </div>
        </div>

        <div class="row maps">
            <div class="col-xs-12">
                <div class="mapwrap">
                    <div id="map"></div>
                </div>
            </div>
        </div>

    </div>
    </section>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeMTmjNoQ_jfqGY7Mv9t8kt93MT_-7hTQ&callback=initMap"
            defer async></script>
    <script src="{{ asset('js/contacts.js') }}"></script>
@endsection