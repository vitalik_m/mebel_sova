@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'pages', $page, $page->slug) }}
@endsection

@section('content')
    <section class="similar-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                {{ Breadcrumbs::render('pages', $page, $page->slug) }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="about-company">
                    <h1 class="about-company__title">{{ $page->title }}</h1>

                    <div class="about-company__text">{!! $page->description !!}</div>
                </div>
            </div>
        </div>
    </div>
    </section>
@endsection