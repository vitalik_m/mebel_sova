@extends('layouts.main')

@section('content')
    <div class="bannersection">
        <div class="arrwrap">
            <div class="row">
                <!-- <a href="#" class="swiper-button-prev"></a>
                <a href="#" class="swiper-button-next"></a> -->
                <div class="swiper-container top_slider">
                    <div class="swiper-wrapper">
{{--                        <div class="swiper-slide top_greenslide">--}}
{{--                            <img alt="титульная" src="{{ asset('storage/templates/images/itypes/f6071bde5311b9be5032690792db5dd1_1526302130.jpg')}}" class="img-responsive" title="титульная">--}}
{{--                        </div>--}}
                        <div class="swiper-slide top_greenslide">
                            <iframe id="tour-3d" src="https://www.google.com/maps/embed?pb=!4v1581945017954!6m8!1m7!1sCAoSLEFGMVFpcE1keXludlJQSkRvWlA2dllCdWhiS3VURHd0b21LSHhFZDB6MUpi!2m2!1d47.8699615!2d35.0703103!3f235.64251368457346!4f-4.172796319706947!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="similar-section">
        <div class="container">
            <div class="row similar-goods">
                <h2 class="similar-goods__header">Cпециальные предложения</h2>

                @foreach($specialOffers as $product)
                <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 similar-goods__itemsimilar">
                    <figure class="similar-goods__item similar-goods__item_action">
                        <a href="{{  productPath($product) }}">
                            <img alt="{{ $product->title }}"
                                 src="{{ asset('storage/' . $product->main_image) }}"
                                 class="img-responsive"
                                 title="{{ $product->title }}">
                        </a>
                    </figure>
                    <h3 class="similar-goods__itemheader"><a href="{{ productPath($product) }}">{{ $product->title }}</a></h3>
                    <a href="{{ productPath($product) }}" class="similar-goods__readmore">Посмотреть</a>
                </div>
                @endforeach

            </div>
        </div>
        <a href="{{ route('pages.actions') }}" class="seeall">Все предложения >></a>
    </section>

    <section class="aboutsection">
        <div class="container aboutsection-items">
            {!! $page->description !!}
        </div>
    </section>
@endsection