<div class="{{$attr->slug}}-filter filter-block" data-slug="{{$attr->slug}}">
    <label>
        <h5><b>{{$attr->title}} @if($attr->unit) ({{ $attr->unit }}) @endif :</b></h5>

        <div class="range-info">
            <div class="left-part">
                <input name="filters[float][{{$attr->id}}][start]" id="{{$attr->slug}}_start" type="number"
                       class="{{$attr->slug}}_range range range-start pull-left"
                       value="{{ $filters ? $filters['float'][$attr->id]['start'] : $attr->values->whereIn('product_id', $category->products->pluck('id'))->min('value') }}"
                       data-min="{{$attr->values->whereIn('product_id', $category->products->pluck('id'))->min('value')}}" >
                {{--               value="{{ old("filters[float][". $attr->slug ."][start]", $attr->values->whereIn('product_id', $products->pluck('id'))->min('value')) }}">--}}
            </div>

            <div class="right-part pull-right">
                <input name="filters[float][{{$attr->id}}][stop]" id="{{$attr->slug}}_stop" type="number"
                   class="{{$attr->slug}}_range range range-stop pull-right"
                   value="{{ $filters ? $filters['float'][$attr->id]['stop'] : $attr->values->whereIn('product_id', $category->products->pluck('id'))->max('value') }}"
                   data-max="{{ $attr->values->whereIn('product_id', $category->products->pluck('id'))->max('value') }}" >
                {{--               value="{{ old("filters[float][". $attr->slug ."][stop]", $attr->values->whereIn('product_id', $products->pluck('id'))->max('value')) }}">--}}
            </div>
        </div>

        <div class="slider-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
            <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
            <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default first"></span>
            <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default last"></span>
        </div>
    </label>
</div>