<div class="{{$attr->slug}}-filter filter-block">
    <label>
        <h5><b>{{ $attr->title }} @if($attr->unit) ({{ $attr->unit }}) @endif :</b></h5>

        <select name="filters[char][{{$attr->slug}}]">
            <option value=""
                @if(empty($filters['char'][$attr->slug]))
                    selected="selected"
                @endif
            >Показать все</option>

            @foreach($attr->values->unique('value_id') as $value)
                <option value="{{$value->value_id}}"
                    @if($filters && $filters['char'] && $filters['char'][$attr->slug]
                        && $filters['char'][$attr->slug] == $value->value_id)
                    selected="selected"
                    @endif
                >
                    {{$value->valueName->name}}
                </option>
            @endforeach
        </select>
    </label>
</div>