@switch($attr->slug)
    @case('cost')
    <div class="cost-filter filter-block" data-slug="cost">
        <label>
            <h5><b>Цена (грн):</b></h5>

            <div class="range-info">
                <div class="left-part">
                    <input name="filters[special][cost][]" id="cost_start" type="number"
                           class="cost_range range range-start"
                           {{--               value="{{ old('filters[special][cost][]', $category->products->min('value')) }}"--}}
                           value="{{ $filters ? $filters['special']['cost'][0] : $category->products->min('cost') }}"
                           data-min="{{$category->products->min('cost')}}">
                </div>

                <div class="right-part pull-right">
                    <input name="filters[special][cost][]" id="cost_stop" type="number"
                           class="cost_range range range-stop pull-right"
                           {{--               value="{{ old('filters[special][cost][]', $category->products->max('value')) }}"--}}
                           value="{{ $filters ? $filters['special']['cost'][1] : $category->products->max('cost') }}"
                           data-max="{{$category->products->max('cost')}}">
                </div>
            </div>

            <div class="slider-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default first"></span>
                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default last"></span>
            </div>
        </label>
    </div>
    @break

    @default
@endswitch