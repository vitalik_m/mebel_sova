@section('header')
    <link href="{{ asset('css/filters.css') }}" rel="stylesheet">
@endsection


<aside class="filter">
    <form action="" id="filter" class="filter">

        @foreach($filtersList as $attr)
            @include('filters.inc.'.$attr->value_type)
        @endforeach

        <button type="submit">Фильтровать</button>
        <button type="button" id="reset-filters">Сбросить</button>
    </form>
</aside>

@section('scripts')
    <script src="{{ asset('js/filters.js') }}"></script>
@endsection