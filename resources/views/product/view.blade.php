@extends('layouts.main')

@section('structureData')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'catalog.product', $product) }}
@endsection

@section('content')
    <section class="similar-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                {{ Breadcrumbs::render('catalog.product', $product) }}
            </div>
        </div>

        <div class="row product product__sm-item-kit-section">
            <div class="text-right">
                <a href="{{ url()->previous() }}" class="link-back"><i class="glyphicon glyphicon-backward"></i>Вернуться к списку товаров</a>
            </div>
            <h1 class="product__title">{{ $product->title }}</h1>

            <div class="col-xs-12 col-sm-6 product__thumbs">
                <?php $product->photo->prepare_to_get($product->photo); ?>
                <div class="swiper-container gallery-top product-gallery-slider">
                    <div class="swiper-wrapper">
                        @foreach($product->photo->images as $key => $img)
                            @if($product->photo->display_as[($loop->iteration - 1)] == 1)
                                <div class="swiper-slide">
                                    <img alt="{{ $product->photo->alts[$key] }}"
                                        src="{{ $img }}"{{-- asset('storage/' . $img) --}}
                                        title="{{ $product->photo->titles[$key] }}"
                                        class="product__image">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>

                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>

                <div class="swiper-container gallery-thumbs product-thumbs-slider">
                    <div class="swiper-wrapper">
                        @foreach($product->photo->images as $key => $img)
                            @if($product->photo->display_as[($loop->iteration - 1)] == 1)
                                <div class="swiper-slide">
                                    <img alt="{{ $product->photo->alts[$key] }}"
                                        src="{{ $img }}"
                                        title="{{ $product->photo->titles[$key] }}"
                                        class="product__image">
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-6 product__description">
                @if($product->getCollections())
                    <div class="col-xs-12 product_collections">
                        <div class="b-list-features">
                            <div class="b-product-descr-feature">
                                <div class="b-feature-descr__title">
                                    <span style="font-size: large; font-family: 'comic sans ms', sans-serif;">Коллекции:
                                        @foreach($product->getCollections() as $key => $collection)
                                            {{ $key ? ', ': '' }}
                                            <a href="{{ categoryPath($collection) }}">{{ $collection->title }}</a>
                                        @endforeach
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                {!! $product->description !!}
            </div>
        </div>

        <div class="row product">
            <div class="col-12">
                @foreach($product->photo->images as $key => $img)
                    @if($product->photo->display_as[($loop->iteration - 1)] == 2)
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 similar-goods__itemsimilar">
                            <figure class="similar-goods__item ">
                                <a href="{{$img}}" class="example-image-link" data-lightbox="example-set">
                                    <img src="{{$img}}" alt="{{ $product->photo->alts[$key] }}" title="{{ $product->photo->titles[$key] }}" class="img-responsive">
                                </a>
                            </figure>
                            <h3 class="similar-goods__itemheader">{{$product->photo->fields_for_subproduct[($loop->iteration - 1)]['title']}}</h3>
                            <div class="similar-goods__name">
                                <p class="similar-goods__size text-center">{{$product->photo->fields_for_subproduct[($loop->iteration - 1)]['key']}}</p>
                                <div class="text-center">{{$product->photo->fields_for_subproduct[($loop->iteration - 1)]['value']}}</div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

    </div>
    </section>

{{--  //TODO subs (related products?)  --}}
{{--    <section class="similar-section sm-item-kit-section">--}}
{{--        <div class="container">--}}
{{--            <div class="row similar-goods">--}}

{{--                <!-- BEGIN sub -->--}}
{{--                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 similar-goods__itemsimilar">--}}
{{--                    <figure class="similar-goods__item ">--}}
{{--                        <a href="/templates/images/items/b_{IMG}" class="example-image-link"--}}
{{--                           data-lightbox="example-set">--}}
{{--                            <img src="/templates/images/items/b_{IMG}" alt="{NAME}" title="{NAME}" class="img-responsive">--}}
{{--                        </a>--}}
{{--                    </figure>--}}
{{--                    <h3 class="similar-goods__itemheader">{NAME}</h3>--}}

{{--                    <!-- BEGIN sub-size-block -->--}}
{{--                    <div class="similar-goods__name">--}}
{{--                        <p class="similar-goods__size text-center">Размер <span class="gray">(выс/шир/гл)</span></p>--}}
{{--                        <div class="text-center">{NAME1}</div>--}}
{{--                    </div>--}}
{{--                    <!-- END sub-size-block -->--}}
{{--                </div>--}}
{{--                <!-- END sub -->--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

    <div class="container">
        <div class="row product product__sm-item-kit-section">
            <a href="{{ url()->previous() }}" class="link-back"><i class="glyphicon glyphicon-backward"></i>Вернуться к списку товаров</a>
        </div>
    </div>
@endsection