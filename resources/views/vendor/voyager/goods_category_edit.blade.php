@extends('voyager::master')
@section('content')
  <div class="container-fluid">
    <div class="page-content">
      <h1 class="page-title">Редактировать категорию</h1>
      <a href="{{ route('voyager.goods_categories.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Новая категория</span>
      </a>
    </div>
    <div class="page-content">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label>
            <span>Название категории</span>
            <input type="text" name="title" placeholder="Название категории" class="form-control" value="{{ $current_category['name'] }}" />
          </label>
        </div>
        <div class="form-group">
          <label>
            <span>Родительский раздел</span>
            <select name="parent_category" class="form-control">
              <option>Выберите родительскую категорию</option>
              @if( is_array($categories) )
                @foreach( (array) $categories as $category)
                  <option value="{{ $category['main']['id'] }}" {{ ($current_category['parent'] == $category['main']['id']) ? 'selected' : '' }} {{ ($current_category['id'] == $category['main']['id']) ? 'disabled' : '' }}>{{ $category['main']['name'] }}</option>
                  @if( !empty($category['sub']) )
                      @foreach($category['sub'] as $subcategory)
                        <option value="{{ $subcategory['id'] }}" disabled>&nbsp;&nbsp;&nbsp;{{ $subcategory['name'] }}</option>
                      @endforeach
                  @endif
                @endforeach
              @endif
          </select>
          </label>
        </div>
        
        <div class="form-group">
          <label>
            <span>ЧПУ ссылка</span>
            <input type="text" name="url" require placeholder="ЧПУ ссылка" class="form-control"  value="{{ $current_category['slug'] }}" />
          </label>
        </div>
        {{--
        <div class="form-group">
          <label>
            <span>Фото</span>
            <input type="file" name="photo" class="form-control-file" />
          </label>
        </div>

        <div class="form-group">
          <label>
            <input type="checkbox" name="enabled" class="form-check-label" />
            <span>Enable category</span>
          </label>
        </div>
        
        <div class="form-group">
          <label>
            <span>Текст</span>
            <textarea id="tiny" name="category_description" class="form-control">description. change this to wisivic edirod</textarea>
          </label>
        </div>

        <hr>

        <div class="form-group">
          <label>
            <span>SEO Title</span>
            <input type="text" name="seo_title" placeholder="SEO Title" class="form-control" />
          </label>
        </div>

        <div class="form-group">
          <label>
            <span>SEO description</span>
            <textarea name="seo_description" placeholder="SEO description" class="form-control" ></textarea>
          </label>
        </div>

        <div class="form-group">
          <label>
            <span>SEO description</span>
            <input type="text" name="seo_keywords" placeholder="SEO keywords" class="form-control" />
          </label>
        </div>
        --}}
        @csrf

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
        
      </form>
    </div>
  </div>
@stop