@php
    $charValue = !empty($curProperties[$propKey]) && !empty($curCharValues[$propKey]) ? $curCharValues[$propKey]->value_id : old('char.'. $property->id .'.value_id' , null);
@endphp
<div class="form-group col-md-4 col-12">
    <div>
        <label class="control-label" for="prop-{{ $propKey }}" >{{ $property->title }}</label>
    </div>

    <div>
        <input type="hidden"
               name="char[{{ $property->id }}][property_id]" value="{{ $property->id }}">

        <select name="char[{{$property->id}}][value_id]">
            @foreach($property->charsValues as $char)
                <option value="{{$char->id}}"  @if(!empty($charValue == $char->id))  selected="selected" @endif>{{$char->name}}</option>
            @endforeach
        </select>
    </div>
</div>