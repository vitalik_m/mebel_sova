@php
    $floatValue = !empty($curProperties[$propKey]) && !empty($curFloatValues[$propKey]) ? $curFloatValues[$propKey]->value : old('float.'. $property->id .'.value' , null);
@endphp
<div class="form-group col-md-4 col-12">
    <div>
        <label class="control-label" for="prop-{{ $property->id }}" >{{ $property->title }}  @if($property->unit) ({{ $property->unit }}) @endif</label>
    </div>
    <div>
        <input type="number" step="0.01" id="prop-{{ $property->id }}"
               name="float[{{ $property->id }}][value]" value="{{ $floatValue }}">
        <input type="hidden"
               name="float[{{ $property->id }}][property_id]" value="{{ $property->id }}">
    </div>
</div>