@extends('voyager::master')
@section('content')
  <div class="container-fluid">
      <div class="page-content">
        <h1 class="page-title">Страницы</h1>
        <a href="{{ route('voyager.pages.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Добавить страницу</span>
      </a>
      </div>
      <div class="page-content">

        @if( !empty($pages) )
          <table class="table">
            <thead>
              <tr>
                <th scope="col">№</th>
                <th scope="col">Название</th>
                <th scope="col">URL</th>
                <th scope="col">Изменить</th>
                <th scope="col">Удалить</th>
              </tr>
            </thead>
            <tbody>
              @foreach($pages as $page)
              <tr>
                <td>{{ $page['id'] }}</td>
                <td>{{ $page['name'] }}</td>
                <td>{{ $page['slug'] }}</td>
                <td><a href="{{ route('voyager.pages.edit_ui', $page['id']) }}">Изменить</a></td>
                <td><a href="{{ route('voyager.page.del', $page['id']) }}">Удалить</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @else
          Нет товаров. <a href="{{ route('voyager.pages.add') }}">Добавить страницу</a>
        @endif

      </div>
  </div>
@stop