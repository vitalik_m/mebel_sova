@extends('voyager::master')
@section('content')
  <div class="container-fluid">
      <div class="page-content">
        <h1 class="page-title">Товары</h1>
        <a href="{{ route('voyager.goods.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Добавить товар</span>
      </a>
      </div>
      <?php
      ?>
      <div class="page-content">
        @if( !empty($goods) )
          <table class="table">
            <thead>
              <tr>
                <th scope="col">№</th>
                <th scope="col">Название</th>
                <th scope="col">URL</th>
                <th scope="col">Изменить</th>
                <th scope="col">Удалить</th>
              </tr>
            </thead>
            <tbody>
              @foreach($goods as $product)
              <tr>
                <td>{{ $product['id'] }}</td>
                <td>{{ $product['title'] }}</td>
                <td>{{ $product['slug'] }}</td>
                <td><a href="{{ route('voyager.goods.edit_ui', $product['id']) }}">Изменить</a></td>
                <td><a href="{{ route('voyager.goods.del', $product['id']) }}">Удалить</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @else
          Нет товаров. <a href="{{ route('voyager.goods.add') }}">Добавить товар</a>
        @endif
      </div>
  </div>
@stop