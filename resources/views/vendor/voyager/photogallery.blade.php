@extends('voyager::master')
@section('content')
  <div class="container-fluid">
      <div class="page-content">
        <h1 class="page-title">Галереи</h1>
        <a href="{{ route('voyager.photogalleries.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Добавить галерею</span>
      </a>
      </div>
      <div class="page-content">
        <table class="table">
        <thead>
          <tr>
            <th scope="col">№</th>
            <th scope="col">Название</th>
            <th scope="col">URL</th>
            <th scope="col">Шорткод</th>
            <th scope="col">Доступные действия</th>
          </tr>
        </thead>
        <tbody>
        @forelse ($galleries as $gallery)
        <tr class="product_category">
          <td>{{ $gallery['id'] }}</td>
          <td>{{ $gallery['title'] }}</td>
          <td>{{ $gallery['shortcode'] }}</td>
          <td><input type="text" value="[{{ $gallery['shortcode'] }}]" /></td>
          <td>
            <a href="{{ route('voyager.photogalleries.del', $gallery['id']) }}" title="Удалить" class="btn btn-sm btn-danger pull-right delete">
              <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Удалить</span>
            </a>
            <a href="{{ route('voyager.photogalleries.edit', $gallery['id']) }}" title="Изменить" class="btn btn-sm btn-primary pull-right edit">
              <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Изменить</span>
            </a>
          </td>
        </tr>
        @empty
        @endforelse
        </tbody>
      </table>
      </div>
  </div>
@stop