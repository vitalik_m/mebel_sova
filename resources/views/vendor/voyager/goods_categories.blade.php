@extends('voyager::master')
@section('content')
  <div class="container-fluid">
    <div class="page-content">
      <h1 class="page-title">Категории</h1>
      <a href="{{ route('voyager.goods_categories.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Новая категория</span>
      </a>
    </div>
    <div class="page-content">
    @if( !empty($categories) )
      <table class="table">
        <thead>
          <tr>
            <th scope="col">№</th>
            <th scope="col">Название</th>
            <th scope="col">URL</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
          </tr>
        </thead>
        <tbody>
        @foreach( (array) $categories as $category)
          <tr class="product_category">
            <td>{{ $category['main']['id'] }}</td>
            <td>{{ $category['main']['name'] }}</td>
            <td>{{ $category['main']['slug'] }}</td>
            <td><a href="{{ route('voyager.goods_categories.edit_ui', $category['main']['id']) }}">edit</a></td>
            <td><a href="{{ route('voyager.goods_categories.del', $category['main']['id']) }}">delete</a></td>
          </tr>
          @if( !empty($category['sub']) )
            @foreach($category['sub'] as $subcategory)
            <tr class="product_subcategory">
              <td>{{ $subcategory['id'] }}</td>
              <td>{{ $subcategory['name'] }}</td>
              <td>{{ $subcategory['slug'] }}</td>
              <td><a href="{{ route('voyager.goods_categories.edit_ui', $subcategory['id']) }}">edit</a></td>
              <td><a href="{{ route('voyager.goods_categories.del', $subcategory['id']) }}">delete</a></td>
            </tr>
            @endforeach
          @endif
        @endforeach
        </tbody>
      </table>
    @else
      Нет категорий. <a href="{{ route('voyager.goods_categories.add') }}">Создать новую категорию</a>
    @endif
    </div>
  </div>
@stop