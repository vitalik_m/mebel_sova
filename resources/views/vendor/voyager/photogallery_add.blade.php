@extends('voyager::master')
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">

            <div class="page-content col-sm-12">
                <h1 class="page-title">Добавить галерею</h1>

                <a href="{{ route('voyager.photogalleries.add') }}" class="btn btn-success btn-add-new">
                    <i class="voyager-plus"></i> <span>Добавить галерею</span>
                </a>
            </div>

            <div class="page-content col-sm-12">
                <form method="post" enctype="multipart/form-data" class="container-fluid">
                    @csrf

                    <div class="row">
                        <div class="form-group col-sm-2">
                            <label>
                                <span>Название</span>
                                <input type="text" name="title" required placeholder="Название" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label>
                                <span>Код для вставки</span>
                                <input type="text" name="shortcode" readonly data-slug-origin="title"
                                       placeholder="Код для вставки" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label>
                                <input type="checkbox" name="enable" checked="checked" class="toggleswitch"/>
                                <span>Включить</span>
                            </label>
                        </div>
                    </div>

                    <div class="row items-block">
                        @include('vendor.voyager.photogallery.inc.item', ['i' => '0', 'number' => 1])
                    </div>

                    <div class="row btns-block">
                        <div class="form-group col-sm-12" id="gallery_add_button">
                            <a id="add_new_file_upload_group" class="btn btn-success btn-add-new">+ Добавить поле</a>
                        </div>

                        <div class="form-group col-sm-12" id="gallery_save_button">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>

                @section('javascript')
                    <script>
                        $('.side-body input[data-slug-origin]').each(function (i, el) {
                            $(el).slugify();
                        });

                        document.querySelectorAll('input[name="gallery_upload_0_display"]').forEach(function (item) {
                            item.addEventListener('change', function (event) {
                                var sub_product_fields_displaying = document.querySelector('.displaying.box_0 .fields_for_subproducts');
                                if (event.target.value == 2) {
                                    // if selected subproduct
                                    sub_product_fields_displaying.style.display = 'block';
                                } else {
                                    sub_product_fields_displaying.style.display = 'none';
                                }
                            });
                        });
                    </script>

                    <script>
                        function previewFile(file) {
                            var file_upload_group = file.parentElement;
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                file_upload_group.querySelector('img').src = reader.result;
                            }

                            if (file) {
                                reader.readAsDataURL(file.files[0]);
                            } else {
                                preview.src = "/storage/templates/no-image.png";
                            }
                        }
                    </script>

                    <script>
                        function clearPreview(clear_button) {
                            var group_num = clear_button.id.replace('clear_upload_img_', '');
                            var preview_img = document.querySelector('#preview_' + group_num);
                            var img_upload = document.querySelector('input[name="gallery_upload_' + group_num + '"]');
                            var deleted_images_list = document.querySelector('input[name="deleted_images"]');

                            preview_img.src = "/storage/templates/no-image.png";
                            img_upload.value = "";

                            deleted_images_list.value += group_num + ',';
                        }
                    </script>

                    <script>
                        var photogallery = {
                            photosCount: 0,

                            init : function(){
                                document.querySelector('#add_new_file_upload_group').addEventListener('click', function (event) { photogallery.addNew(event)});
                            },

                            addNew: function(event){
                                event.preventDefault();
                                photogallery.countPhotos();

                                var itemsBlock = document.querySelector('.items-block');

                                var group = document.createElement("div");
                                group.innerHTML = `@include('vendor.voyager.photogallery.inc.item', ['i' => '` + photogallery.photosCount + `', 'image' => null])`;
                                itemsBlock.appendChild(group.firstElementChild);

                                var all_blocks = document.querySelectorAll('.displaying');
                                var last_block = all_blocks[all_blocks.length - 1];
                                var last_index = last_block.classList.item(1).replace('box_', '');
                                document.querySelectorAll('input[name="gallery_upload_' + last_index + '_display"]').forEach(function (item) {
                                    item.addEventListener('change', function (event) {
                                        var sub_product_fields_displaying = document.querySelector('.displaying.box_' + last_index + ' .fields_for_subproducts');
                                        if (event.target.value == 2) {
                                            // if selected "subproduct"
                                            sub_product_fields_displaying.style.display = 'block';
                                        } else {
                                            sub_product_fields_displaying.style.display = 'none';
                                        }
                                    });
                                });
                            },

                            countPhotos() {
                                photogallery.photosCount = document.querySelectorAll('.items-block>.form-group.image').length;
                            }
                        };

                        photogallery.init();
                    </script>
                @endsection
            </div>

        </div>
    </div>
@stop