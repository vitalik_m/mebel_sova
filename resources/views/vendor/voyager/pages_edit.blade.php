@extends('voyager::master')
@section('content')
  <div class="container-fluid">
      <div class="page-content">
        <h1 class="page-title">Добавить страницу</h1>
        <a href="{{ route('voyager.pages.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Добавить страницу</span>
      </a>
      </div>

      <div class="page-content">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label>
                <span>Название</span>
                <input type="text" name="title" required value="{{ $page['name'] }}" placeholder="Название" class="form-control" />
              </label>
            </div>

            <div class="form-group">
              <label>
                <span>Тип</span>
                <select name="page_type" class="form-control">
                  <option>Тип</option>
                  @if( is_array($page_types) )
                    @foreach( (array) $page_types as $page_type)
                      <option value="{{ $page_type['id'] }}" {{ ($page['type'] == $page_type['id']) ? 'selected' : '' }}>{{ $page_type['name'] }}</option>
                    @endforeach
                  @endif
              </select>
              </label>
            </div>
            
            <div class="form-group">
              <label>
                <span>ЧПУ ссылка</span>
                <input type="text" name="url" value="{{ $page['slug'] }}" placeholder="ЧПУ ссылка" class="form-control" />
              </label>
            </div>

            <div class="form-group">
              <label>
                <input type="checkbox" name="enabled" {{ ($page['enabled'] == 1) ? 'checked' : '' }} class="form-check-label" />
                <span>Отображать на сайте</span>
              </label>
            </div>
            
            <div class="form-group">
              <label>
                <span>Текст</span>
                <textarea id="tiny" name="description" class="form-control">{{ $page['description'] }}</textarea>
              </label>
            </div>

            <hr>

            <div class="form-group">
              <label>
                <span>SEO Title</span>
                <input type="text" name="seo_title" value="{{ $page['seo_title'] }}" placeholder="SEO Title" class="form-control" />
              </label>
            </div>

            <div class="form-group">
              <label>
                <span>SEO description</span>
                <textarea name="seo_description" placeholder="SEO description" class="form-control" >{{ $page['seo_description'] }}</textarea>
              </label>
            </div>

            <div class="form-group">
              <label>
                <span>SEO description</span>
                <input type="text" name="seo_keywords" value="{{ $page['seo_keywords'] }}" placeholder="SEO keywords" class="form-control" />
              </label>
            </div>

            <div class="form-group">
              <label>
                <span>Класс меню</span>
                <input type="text" name="menu_class" placeholder="Класс меню" class="form-control" />
              </label>
            </div>

            @csrf

            <div class="form-group">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
      </div>
  </div>
@stop