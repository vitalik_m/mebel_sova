@extends('voyager::master')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="page-content col-sm-12">
                <h1 class="page-title">Редактировать галерею</h1>
            </div>

            <div class="page-content">
                <form method="post" enctype="multipart/form-data" class="container-fluid">
                    @csrf

                    <div class="row">
                        <div class="form-group col-sm-2">
                            <label>
                                <span>Название</span>
                                <input type="text" name="title" value="{{ $gallery['title'] }}" required
                                       placeholder="Название" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-md-2">
                            <label>
                                <span>Код для вставки</span>
                                <input type="text" name="shortcode" readonly data-slug-origin="title"
                                       placeholder="Код для вставки" class="form-control"/>
                            </label>
                        </div>

                        <div class="form-group col-md-2">
                            <label>
                                <input type="checkbox" name="enable"
                                       {{ ($gallery['enabled'] ? 'checked="checked"' : '') }} class="toggleswitch"/>
                                <span>Включить</span>
                            </label>
                        </div>

                        <div>
                            <input type="hidden" name="deleted_images"/>
                        </div>
                    </div>

                    <div class="row items-block">
                    @forelse ($gallery['images'] as $key => $image)
                        @include('vendor.voyager.photogallery.inc.item', ['i' => $key, 'number' => $key+1])
                    @empty
                        @include('vendor.voyager.photogallery.inc.item', ['i' => '0'])
                    @endforelse
                    </div>

                    <div class="form-group" id="gallery_add_button">
                        <a id="add_new_file_upload_group" class="btn btn-success btn-add-new">+ Добавить поле</a>
                    </div>

                    <div class="form-group" id="gallery_save_button">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>

                @section('javascript')
                <script>
                    $('.side-body input[data-slug-origin]').each(function (i, el) {
                        $(el).slugify();
                    });
                    document.addEventListener('DOMContentLoaded', function (event) {
                        document.querySelector('input[name="shortcode"]').value = "{{ $gallery['shortcode'] }}";

                        var exist_boxes = document.querySelectorAll('.displaying');
                        exist_boxes.forEach(function (block) {
                            var value = block.classList[1].replace('box_', '');
                            block.querySelectorAll('input[name="gallery_upload_' + value + '_display"]').forEach(function (item) {
                                item.addEventListener('change', function (event) {
                                    var sub_product_fields_displaying = document.querySelector('.displaying.box_' + value + ' .fields_for_subproducts');
                                    if (event.target.value == 2) {
                                        // if selected subproduct
                                        sub_product_fields_displaying.style.display = 'block';
                                    } else {
                                        sub_product_fields_displaying.style.display = 'none';
                                    }
                                });
                                if (item.value == 2 && item.checked) {
                                    document.querySelector('.displaying.box_' + value + ' .fields_for_subproducts').style.display = 'block'
                                }
                            });
                        });

                        document.querySelectorAll('input[name="gallery_upload_0_display"]').forEach(function (item) {
                            item.addEventListener('change', function (event) {
                                var sub_product_fields_displaying = document.querySelector('.displaying.box_0 .fields_for_subproducts');
                                if (event.target.value == 2) {
                                    // if selected subproduct
                                    sub_product_fields_displaying.style.display = 'block';
                                } else {
                                    sub_product_fields_displaying.style.display = 'none';
                                }
                            });
                        });
                    })
                </script>

                <script>
                    function previewFile(file) {
                        var file_upload_group = file.parentElement;
                        var reader = new FileReader();
                        var group_num = file.name.replace('gallery_upload_', '');

                        reader.onloadend = function () {
                            file_upload_group.querySelector('img').src = reader.result;
                            var deleted_images_list = document.querySelector('input[name="deleted_images"]');
                            var delete_group_from_deleted_list = new RegExp('/\b' + group_num + '\,\b/');
                            deleted_images_list.value.replace(delete_group_from_deleted_list, '');
                        }

                        if (file) {
                            reader.readAsDataURL(file.files[0]);
                        } else {
                            preview.src = "/storage/templates/no-image.png";
                        }
                    }
                </script>

                <script>
                    function clearPreview(clear_button) {
                        var group_num = clear_button.id.replace('clear_upload_img_', '');
                        var preview_img = document.querySelector('#preview_' + group_num);
                        var img_upload = document.querySelector('input[name="gallery_upload_' + group_num + '"]');
                        var deleted_images_list = document.querySelector('input[name="deleted_images"]');

                        preview_img.src = "/storage/templates/no-image.png";
                        img_upload.value = "";

                        deleted_images_list.value += group_num + ',';
                    }
                </script>

                <script>
                    var photogallery = {
                        photosCount: 0,

                        init : function(){
                            document.querySelector('#add_new_file_upload_group').addEventListener('click', function (event) { photogallery.addNew(event)});
                        },

                        addNew: function(event){
                            event.preventDefault();
                            photogallery.countPhotos();

                            var itemsBlock = document.querySelector('.items-block');

                            var group = document.createElement("div");
                            group.innerHTML = `@include('vendor.voyager.photogallery.inc.item', ['i' => '` + photogallery.photosCount + `', 'image' => null])`;
                            itemsBlock.appendChild(group.firstElementChild);

                            var all_blocks = document.querySelectorAll('.displaying');
                            var last_block = all_blocks[all_blocks.length - 1];
                            var last_index = last_block.classList.item(1).replace('box_', '');
                            document.querySelectorAll('input[name="gallery_upload_' + last_index + '_display"]').forEach(function (item) {
                                item.addEventListener('change', function (event) {
                                    var sub_product_fields_displaying = document.querySelector('.displaying.box_' + last_index + ' .fields_for_subproducts');
                                    if (event.target.value == 2) {
                                        // if selected "subproduct"
                                        sub_product_fields_displaying.style.display = 'block';
                                    } else {
                                        sub_product_fields_displaying.style.display = 'none';
                                    }
                                });
                            });
                        },

                        countPhotos() {
                            photogallery.photosCount = document.querySelectorAll('.items-block>.form-group.image').length;
                        }
                    }
                    photogallery.init();
                </script>
            @endsection
            </div>

        </div>
    </div>
@stop