@extends('voyager::master')
@section('content')
  <div class="container-fluid">
      <div class="page-content">
        <h1 class="page-title">Редактировать товар</h1>
        <a href="{{ route('voyager.goods.add') }}" class="btn btn-success btn-add-new">
          <i class="voyager-plus"></i> <span>Добавить товар</span>
      </a>
      </div>
      <div class="page-content">
        <form method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label>
              <span>Название</span>
              <input type="text" name="title" value="{{ $product['title'] }}" placeholder="Название" class="form-control" />
            </label>
          </div>

          <div class="form-group">
            <label>
              <span>Категория</span>
              <select name="category" class="form-control">
                <option>Категория</option>
                @if( is_array($categories) )
                  @foreach( (array) $categories as $category)
                    <option value="{{ $category['main']['id'] }}" {{ ($product['product_category'] == $category['main']['id']) ? 'selected' : '' }}>{{ $category['main']['name'] }}</option>
                    @if( !empty($category['sub']) )
                        @foreach($category['sub'] as $subcategory)
                          <option value="{{ $subcategory['id'] }}" {{ ($product['product_category'] == $subcategory['id']) ? 'selected' : '' }}>&nbsp;&nbsp;&nbsp;{{ $subcategory['name'] }}</option>
                        @endforeach
                    @endif
                  @endforeach
                @endif
            </select>
            </label>
          </div>
          
          <div class="form-group">
            <label>
              <span>ЧПУ ссылка</span>
              <input type="text" name="url" value="{{ $product['slug'] }}" placeholder="ЧПУ ссылка" class="form-control" />
            </label>
          </div>

          <div class="form-group">
            <label>
              <span>Фото</span>
              <input type="file" name="photo" data-name="media_picker" class="form-control-file" />

              
            </label>
<?php
dump(get_class_methods(Voyager::class));

//$dt = TCG\Voyager\Models\DataType();
//dump($dt->fields());
$v = new \TCG\Voyager\Voyager();
dump(get_class_methods($v));
dump($v->actions());
?>
            
            @include('voyager::alerts')
            {{--
              <div id="filemanager">
                <media-manager
                base-path="{{ config('voyager.media.path', '/') }}"
                :show-folders="{{ config('voyager.media.show_folders', true) ? 'true' : 'false' }}"
                :allow-upload="{{ config('voyager.media.allow_upload', true) ? 'true' : 'false' }}"
                :allow-move="{{ config('voyager.media.allow_move', true) ? 'true' : 'false' }}"
                :allow-delete="{{ config('voyager.media.allow_delete', true) ? 'true' : 'false' }}"
                :allow-create-folder="{{ config('voyager.media.allow_create_folder', true) ? 'true' : 'false' }}"
                :allow-rename="{{ config('voyager.media.allow_rename', true) ? 'true' : 'false' }}"
                :allow-crop="{{ config('voyager.media.allow_crop', true) ? 'true' : 'false' }}"
                :details="{{ json_encode(['thumbnails' => config('voyager.media.thumbnails', []), 'watermark' => config('voyager.media.watermark', (object)[])]) }}"
                ></media-manager>
              </div>
            
            @section('javascript')
              <script>
              new Vue({
                  el: '#filemanager'
              });
              </script>
            @endsection
            --}}
            <?php
              //dump(get_class_methods(Voyager::class));
              //dump(Voyager::partialMock()->formFields()->view());
            ?>
          </div>

          <div class="form-group">
            <label>
              <input type="checkbox" name="special_offer" {{ ( (bool) $product['special_offer']) ? 'checked' : '' }} class="form-check-label" />
              <span>Специальное предложение</span>
            </label>
          </div>

          <div class="form-group">
            <label>
              <input type="checkbox" name="enabled" {{ ( (bool) $product['enable']) ? 'checked' : '' }} class="form-check-label" />
              <span>Отображать на сайте</span>
            </label>
          </div>
          
          <div class="form-group">
            <label>
              <span>Текст</span>
              <textarea id="tiny" name="description" class="form-control">{{ $product['description'] }}</textarea>
            </label>
          </div>

          <hr>

          <div class="form-group">
            <label>
              <span>SEO Title</span>
              <input type="text" name="seo_title" value="{{ $product['seo_title'] }}" placeholder="SEO Title" class="form-control" />
            </label>
          </div>

          <div class="form-group">
            <label>
              <span>SEO description</span>
              <textarea name="seo_description" value="{{ $product['seo_description'] }}" placeholder="SEO description" class="form-control" ></textarea>
            </label>
          </div>

          <div class="form-group">
            <label>
              <span>SEO description</span>
              <input type="text" name="seo_keywords" value="{{ $product['seo_keywords'] }}" placeholder="SEO keywords" class="form-control" />
            </label>
          </div>

          @csrf

          <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
          </div>
          
        </form>
      </div>
  </div>
@stop