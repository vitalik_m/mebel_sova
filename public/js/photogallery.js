var photogallery = {
    init : function(){
        photogallery.functs.slugify();
        photogallery.functs.setShortcode();

        document.querySelectorAll('.items-block .form-group.image ').forEach(function(item, i){
            photogallery.functs.eventDisplayType(item);
        });

        document.querySelector('#add_new_file_upload_group')
            .addEventListener('click', function (event) { photogallery.addNew(event)});

        document.querySelectorAll('.view_type_block>label>input').forEach(function(item){
            item.addEventListener('change', function (event) { photogallery.toggleDisplayAs(this)});
        });

        photogallery.order.init();
    },

    addNew: function(event){
        event.preventDefault();

        var itemsBlock = document.querySelector('.items-block');

        var group = document.createElement("div");
        group.innerHTML = photogallery_data.itemTemp();
        itemsBlock.appendChild(group.firstElementChild);

        let last_item = photogallery.get.last_item();
        photogallery.functs.eventDisplayType(last_item);
        photogallery.order.events( photogallery.get.curItem(last_item) );
    },

    deleteItem: function(del_button) {
        let deletingItem = photogallery.get.curItem(del_button);
        photogallery.deletingFuncts.toggleMarkDeleting(deletingItem);

        if(!deletingItem.dataset.isnew){
            photogallery.deletingFuncts.delToggleFromDB(deletingItem);
        }else{
            photogallery.deletingFuncts.delDom(deletingItem);
        }
    },

    //it is for right value after moving items
    toggleDisplayAs: function(radio){
        const galleryClass = 'as-gallery';
        const subProductClass = 'as-subproduct';
        let curItem = photogallery.get.curItem(radio);

        if(radio.classList.contains(galleryClass)){
            curItem.querySelector('.'+subProductClass).removeAttribute('checked');
            radio.setAttribute('checked', true);
            radio.checked = true;
        }else{
            curItem.querySelector('.'+galleryClass).removeAttribute('checked');
            radio.setAttribute('checked', true);
            radio.checked = true;
        }
    },

    deletingFuncts: {
        deletingClass: 'deleting',
        timeShowingMsg: 2,
        timeBeforeDeletingDom: 3,
        delAjaxFromDB: function(deletingItem){
            photogallery.deletingFuncts.toggleLoader(deletingItem);

            let token = $("meta[name='csrf-token']").attr("content");
            let id = photogallery.get.currentIndex( deletingItem );
            let delLink = photogallery_data.delItemLink.replace('item_id', id);

            $.ajax({
                url: delLink,
                type: 'post',
                dataType: "JSON",
                data: {
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function (response)
                {
                    if(response.status == 'ok'){
                        photogallery.deletingFuncts.showMsg(deletingItem, response.status, response.msg);
                        photogallery.deletingFuncts.delDom(deletingItem);
                    }else{
                        photogallery.deletingFuncts.toggleMarkDeleting(deletingItem);
                        let errMsg = response.msg ?? photogallery_data.msgs.deleteFail;
                        photogallery.deletingFuncts.showMsg(deletingItem, response.status, errMsg);
                    }
                },
                error: function (response)
                {
                    photogallery.deletingFuncts.toggleMarkDeleting(deletingItem);
                    let errMsg = response.msg ?? photogallery_data.msgs.deleteFail;
                    photogallery.deletingFuncts.showMsg(deletingItem, 'fail', errMsg);
                }
            });
        },
        delToggleFromDB: function(deletingItem){
            //TODO to finish toggeling
            let id = photogallery.get.currentIndex( deletingItem );

            let group_num = photogallery.get.currentIndex(deletingItem);
            let deleted_images_list = document.querySelector('input[name="deleted_images"]');

            deleted_images_list.value += group_num + ',';
        },
        toggleMarkDeleting: function(item){
            if(item.classList.contains(photogallery.deletingFuncts.deletingClass)){
                item.classList.remove(photogallery.deletingFuncts.deletingClass);
            }else{
                item.classList.add(photogallery.deletingFuncts.deletingClass);
            }
        },
        toggleLoader: function(item, action){
            if(action){
                if(action === 'add'){
                    item.insertAdjacentHTML('afterbegin', photogallery_data.loader);
                }else if(action === 'remove'){
                    item.querySelector('.floatingBarsG').remove();
                }
            }

            if(item.classList.contains('.floatingBarsG')){
                item.querySelector('.floatingBarsG').remove();
            }else{
                item.insertAdjacentHTML('afterbegin', photogallery_data.loader);
            }
        },
        delDom: function(item){
            setTimeout(function () {
                $(item).hide( 300,function(){
                    item.remove();
                    // photogallery.functs.moveItems(
                    //     photogallery.get.currentIndex(item),
                    //
                    // );
                });
            }, photogallery.deletingFuncts.timeBeforeDeletingDom);
        },
        showMsg: function(item, status, msg){
            if(!status) status = 'success';
            status = status == 'ok' ? 'success' : 'fail' ;

            let html = '<div id="item_'+item.dataset.index+'_msg" class="item-msg '+status+'">'+ msg +'</div>';

            item.insertAdjacentHTML('afterbegin', html);
            $('#'+'item_'+item.dataset.index+'_msg').show(300);

            setTimeout(function () {
                $('#'+'item_'+item.dataset.index+'_msg').hide(300);
            }, photogallery.deletingFuncts.timeShowingMsg);
        }
    },

    functs: {
        slugify(){
            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });
        },
        setShortcode() {
            if(!photogallery_data.shortcode) return;

            document.querySelector('input[name="shortcode"]').value = photogallery_data.shortcode;
        },
        eventDisplayType(item) {
            let index = photogallery.get.currentIndex(item);

            document.querySelectorAll('input[name="gallery_upload_' + index + '_display"]').forEach(function (item) {

                item.addEventListener('change', function (event) {
                    let curIndex = photogallery.get.currentIndex( photogallery.get.curItem(event.target));
                    let sub_product_fields_displaying = document.querySelector('.displaying.box_' + curIndex + ' .fields_for_subproducts');
                    let display = parseInt(event.target.value) === 2 && item.checked ? 'block' : 'none';

                    sub_product_fields_displaying.style.display = display;
                });

            });
        },
        previewFile: function(file) {
            var file_upload_group = file.parentElement;
            var reader = new FileReader();
            var group_num = file.name.replace('gallery_upload_', '');

            reader.onloadend = function () {
                file_upload_group.querySelector('img').src = reader.result;

                var deleted_images_list = document.querySelector('input[name="deleted_images"]');
                if(!deleted_images_list) return;
                var delete_group_from_deleted_list = new RegExp('/\b' + group_num + '\,\b/');
                deleted_images_list.value.replace(delete_group_from_deleted_list, '');
            };

            if (file) {
                reader.readAsDataURL(file.files[0]);
            } else {
                preview.src = "/storage/templates/no-image.png";
            }
        },
        clearPreview: function(del_button) {
            // let group_num = del_button.id.replace('clear_upload_img_', '');
            // let preview_img = document.querySelector('#preview_' + group_num);
            // let img_upload = document.querySelector('input[name="gallery_upload_' + group_num + '"]');
            // let deleted_images_list = document.querySelector('input[name="deleted_images"]');
            //
            // preview_img.src = "/storage/templates/no-image.png";
            // img_upload.value = "";
            //
            // deleted_images_list.value += group_num + ',';
        },
        moveItems: function(totalFromIndex, totalToIndex, action) {
            action = !action || '-' ? '-' : '+';

            let nextElemExists = true;
            let fromIndex = totalFromIndex;
            let toIndex;
            while (nextElemExists){
                nextElemExists = document.querySelector('[data-index="'+ fromIndex +'"]');
                toIndex = action == '-'? fromIndex-1 : fromIndex+1;

                photogallery.order.functs.changeIndex(nextElemExists, fromIndex, toIndex);

                fromIndex = action == '-'? fromIndex-- : fromIndex++;
                nextElemExists = document.querySelector('[data-index="'+ fromIndex +'"]');
            }
        }
    },

    get: {
        currentIndex: function(item) {
            return parseInt(item.querySelector('.clear_upload_img').id.match(/[0-9]+/)[0]);
        },
        last_item: function() {
            var all_blocks = document.querySelectorAll('.form-group.image');

            return all_blocks[all_blocks.length - 1];
        },
        curItem: function(internalEl){
            return internalEl.closest('.form-group.image');
        },
        countPhotos() {
            return document.querySelectorAll('.items-block>.form-group.image').length;
        },
    },

    order: {
        init: function(){
            document.querySelectorAll('.items-block .form-group.image').forEach(function(item, i){
                photogallery.order.events(item);
            });
        },

        functs: {
            isFirst: function(item){
                return item.previousElementSibling == null;
            },
            isLast: function(item){
                return item.nextElementSibling == null;
            },
            changeIndex: function(item, fromIndex, toIndex) {
                item.querySelector('.gallery_upload_old_img').name = 'gallery_upload_'+ toIndex +'_old_img';

                item.querySelector('.index_number').value = 1+toIndex;
                item.querySelector('#preview_'+fromIndex).id = 'preview_' + toIndex;
                item.querySelector('input[name=\'gallery_upload_'+fromIndex+'\'').name = 'gallery_upload_' + toIndex;
                item.querySelector('#clear_upload_img_'+fromIndex).id = 'clear_upload_img_' + toIndex;

                let box = item.querySelector('.box_'+fromIndex);
                box.classList.add('box_' + toIndex);
                box.classList.remove('box_' + fromIndex);

                item.querySelector('input.gallery_upload_alt').name = 'gallery_upload_'+ toIndex +'_alt';
                item.querySelector('input.gallery_upload_title').name = 'gallery_upload_'+ toIndex +'_title';

                item.querySelector('input.as-gallery').name = 'gallery_upload_'+ toIndex +'_display';
                item.querySelector('input.as-subproduct').name = 'gallery_upload_'+ toIndex +'_display';

                item.querySelector('input[name=\'title_for_'+fromIndex+'_sub_0\'').name = 'title_for_'+ toIndex +'_sub_0';
                item.querySelector('input[name=\'key_for_'+fromIndex+'_sub_0\'').name = 'key_for_'+ toIndex +'_sub_0';
                item.querySelector('input[name=\'value_for_'+fromIndex+'_sub_0\'').name = 'value_for_'+ toIndex +'_sub_0';
            },
            crutchForRadio: function(){
                document.querySelectorAll('.items-block .form-group.image .as-subproduct').forEach(function(item, i){
                    if(!item.attributes.checked){
                        photogallery.get.curItem(item).querySelector('.as-gallery').checked = true;
                    }else{
                        item.checked = true;
                    }
                });
            }
        },


        move:{
            prev: function(item){
                if(photogallery.order.functs.isFirst(item)) return;

                let oldIndex = photogallery.get.currentIndex(item);
                let newIndex = oldIndex-1;
                let prevElem = item.previousElementSibling;

                photogallery.order.functs.changeIndex(item, oldIndex, newIndex);
                photogallery.order.functs.changeIndex(prevElem, newIndex, oldIndex);

                prevElem.before(item);
                photogallery.order.functs.crutchForRadio();
            },
            next: function(item){
                if(photogallery.order.functs.isLast(item)) return;

                let oldIndex = photogallery.get.currentIndex(item);
                let newIndex = oldIndex+1;
                let nextElem = item.nextElementSibling;

                photogallery.order.functs.changeIndex(item, oldIndex, newIndex);
                photogallery.order.functs.changeIndex(nextElem, newIndex, oldIndex);

                nextElem.after(item);
                photogallery.order.functs.crutchForRadio();
            },
        },

        events: function(item){
            item.querySelector('.arrow-btn-left').addEventListener('click', function(src) {
                photogallery.order.move.prev( photogallery.get.curItem(src.target) );
            });
            item.querySelector('.arrow-btn-right').addEventListener('click', function(src) {
                photogallery.order.move.next( photogallery.get.curItem(src.target) );
            });
        }
    }
};

document.addEventListener('DOMContentLoaded', function (event) {
    photogallery.init();
});