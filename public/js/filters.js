var filters = {
    init: function() {
        filters.range_sliders.init();

        $('button#reset-filters').click(function(){ filters.reset() });
    },

    range_sliders: {
        rangeSlidersSelector: 'form#filter .slider-range',
        init: function() {
            $( filters.range_sliders.rangeSlidersSelector ).each(function(){
                let slug = filters.range_sliders.get.currentSlug(this);

                filters.range_sliders.changing.start(slug);
                filters.range_sliders.changing.stop(slug);

                $(this).slider({
                    range: true,
                    min: filters.range_sliders.get.start(slug),
                    max: filters.range_sliders.get.stop(slug),
                    values: [
                        filters.range_sliders.get.startVal(slug),
                        filters.range_sliders.get.stopVal(slug)
                    ],
                    slide: function( event, ui ) {
                        $( "#"+slug+"_start" ).val(ui.values[ 0 ]);
                        $( "#"+slug+"_stop" ).val(ui.values[ 1 ]);
                    }
                });
            });
        },

        get:{
            currentSlug: function(el) {
                return $(el).closest('.filter-block').data('slug');
            },
            start: function(slug){
                return parseInt( $('#'+slug+'_start.range-start').data('min') );
            },
            stop: function(slug){
                return parseInt( $('#'+slug+'_stop.range-stop').data('max') );
            },
            startVal: function(slug){
                let val = $('#'+slug+'_start.range-start').val();
                return parseInt(val) /*?? range_sliders.get.start(slug)*/;
            },
            stopVal: function(slug){
                let val = $('#'+slug+'_stop.range-stop').val();
                return parseInt(val) /*?? range_sliders.get.stop(slug)*/;
            }
        },

        changing: {
            start: function(slug){
                $("#"+slug+"_start").change(function() {
                    $('.'+slug +"-filter .slider-range").slider('values',0,$(this).val());
                });
            },
            stop: function(slug){
                $("#"+slug+"_stop").change(function() {
                    $('.'+slug +"-filter .slider-range").slider('values',1,$(this).val());
                });
            }
        },

        resetAll: function() {
            $( filters.range_sliders.rangeSlidersSelector ).each(function(){
                let elemL = $(this).siblings('.range-info').find('.left-part input');
                let elemR = $(this).siblings('.range-info').find('.right-part input');

                elemR.val(elemR.data().max);
                elemL.val(elemL.data().min);

                $(this).find('.first').css('left', '0%');
                $(this).find('.last').css('left', '100%');
            });
        }
    },

    selects:{
        selectsSelector: 'form#filter select',
        resetAll: function() {
            $( filters.selects.selectsSelector ).each(function(){
                this.selectedIndex = 0
            });
        }
    },

    reset: function () {
        filters.selects.resetAll();
        filters.range_sliders.resetAll();

        let url = new URL(window.location.href);
        let get = new URLSearchParams(url.search);
        // if(get.has('filters')){
            url.search = ''; //here should be deleting filters :)
        // }

        window.location.href = url;
    }
};

$(document).ready(filters.init());