tinymce.init({
    selector: 'textarea#tiny',
    imagetools_cors_hosts: ['localhost'],
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    }
});

tinymce.init({
    selector: 'div#tiny textarea.form-control',
    height: 500,
    plugins: 'table image imagetools code',
    menubar: 'file | edit | view | insert | format | table| tools',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    }
});

// below tinyMCE for settings
tinymce.init({
    selector: '#site textarea',
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    }
});
