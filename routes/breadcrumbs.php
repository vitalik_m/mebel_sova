<?php

use App\ProductsCategory;

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Главная', route('home'));
});

// Home > pages
Breadcrumbs::for('pages', function ($trail, $page, $static = '') {
    $trail->push('Главная', route('home'));

    if($static) {
        $trail->push($page->title, route('pages.static', ['static' => $static]));
    }else{
        $trail->push($page->title, route('pages.' . $page));
    }
});
// Home > search
Breadcrumbs::for('search', function ($trail) {
    $trail->push('Главная', route('home'));
    $trail->push('Поиск');
});

// Home > question
Breadcrumbs::for('question', function ($trail) {
    $trail->push('Главная', route('home'));
    $trail->push('Задать вопрос', route('question.show'));
});

// Home > articles
Breadcrumbs::for('articles.list', function ($trail) {
    $trail->push('Главная', route('articles.list'));
    $trail->push('Статьи', route('articles.list'));
});
Breadcrumbs::for('articles.view', function ($trail, $article) {
    $trail->push('Home', route('home'));
    $trail->push('Статьи', route('articles.view', ['article' => $article->slug]));
    $trail->push($article->title, route('articles.view', ['article' => $article->slug]));
});

// Home > catalog
Breadcrumbs::for('catalog.categories', function ($trail) {
    $trail->push('Главная', route('home'));
    $trail->push('Каталог', route('catalog.categories'));
});
Breadcrumbs::for('catalog.category', function ($trail, $category) {
    $trail->push('Главная', route('home'));
    $trail->push('Каталог', route('catalog.categories'));

    if($category->parent){
        $trail->push($category->parent->title, categoryPath($category->parent));
    }
    $trail->push($category->title, categoryPath($category));
});
Breadcrumbs::for('catalog.product', function ($trail, $product) {
    $trail->push('Главная', route('home'));
    $trail->push('Каталог', route('catalog.categories'));

    $arrUri = explode('/', request()->path());
    $categorySlug = $arrUri[count($arrUri)-3];

    $category = (new ProductsCategory())->where('slug', $categorySlug)->firstOrFail();
    if($category){
        $trail->push($category->parent->title, categoryPath($category->parent));
    }
    $trail->push($category->title, categoryPath($category));

    $trail->push($product->title, '');
});