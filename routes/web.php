<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('/');
Route::get('/', 'PageController@home')->name('home');

Route::name('pages.')->group(function () {

    Route::get('/actions', 'PageController@actions')->name('actions');
    Route::get('/photo-gallery', 'PageController@static')->name('gallery');
    Route::get('/contacts', 'PageController@contacts')->name('contacts');

    Route::get('/about', 'PageController@static')->name('about');
    Route::get('/services', 'PageController@static')->name('services');
    Route::get('/pay-delivery', 'PageController@static')->name('pay-delivery');

    Route::get('/page/{static}', 'PageController@static')->name('static');

    Route::get('/search', 'PageController@search')->name('search');
});

Route::prefix('/question')->name('question.')->group(function () {
    Route::get('/', 'QuestionController@show')->name('show');
    Route::post('/send', 'QuestionController@send')->name('send');
});

Route::name('articles.')->group(function () {
   Route::get('/articles', 'ArticleController@list')->name('list');
   Route::get('/articles/{article}', 'ArticleController@view')->name('view');
});

Route::name('catalog.')->prefix('/catalog')->group(function () {
    Route::get('/{categoriesURI}/product/{product}', 'ProductController@view')
        ->where('categoriesURI', '.+')->where('product', '.+')->name('product');

    Route::get('/{categoriesURI}/{category}', 'ProductCategoryController@subView')
        ->where(['categoriesURI' => '.+', 'category' => '.+'])->name('subcategory');
    Route::get('/{category}', 'ProductCategoryController@view')->where('category', '.+')
        ->name('category');
    Route::get('/', 'ProductCategoryController@catalog')->name('categories');
});

Route::get('/{any}', 'URLsController@index')->where('any', '^(?!admin|sitemap\.xml).*');
Route::get('/sitemap.xml', 'SitemapController@index');

Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'products', 'namespace' => 'Admin', 'middleware' => 'admin.user'], function () {
        Route::put('{id}/product-properties/update', 'ProductController@updateProperties')
            ->name('admin.products.product-properties.update');

        Route::get('properties', 'ProductPropertyController@index')->name('admin.products.properties');
        Route::post('properties/store', 'ProductPropertyController@store')->name('admin.products.property.store');
        Route::put('properties/update', 'ProductPropertyController@update')->name('admin.products.property.update');
        Route::delete('properties/delete', 'ProductPropertyController@delete')->name('admin.products.property.delete');

        Route::get('values/{property}', 'ProductPropertyCharController@index')->name('admin.products.property.value.char.names');
        Route::get('values', 'ProductPropertyCharController@all')->name('admin.products.property.value.char.all-names');
        Route::post('value/store', 'ProductPropertyCharController@store')->name('admin.products.property.value.char.name.store');
        Route::put('value/update', 'ProductPropertyCharController@update')->name('admin.products.property.value.char.name.update');
        Route::delete('value/delete', 'ProductPropertyCharController@delete')->name('admin.products.property.value.char.name.delete');
    });

    Route::group(['prefix' => 'categories', 'namespace' => 'Admin', 'middleware' => 'admin.user'], function () {
        Route::put('{id}/product-properties/update', 'ProductCategoryController@updateProductProperties')
            ->name('admin.categories.product-properties.update');
    });

    Voyager::routes();

    Route::resource('photogallery', 'Admin\PhotogalleryController', ['as' => 'admin', 'middleware' => 'admin.user']);
    Route::delete('photogallery/{photogallery}/{id}', 'Admin\PhotogalleryController@destroyItem')->name('admin.photogallery.item.delete');
});

