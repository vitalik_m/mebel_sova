<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPropertyCharsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_property_chars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('value_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('product_property_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_property_chars');
    }
}
