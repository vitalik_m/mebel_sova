<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pages')) {
            Schema::create('pages', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('title');
                $table->string('slug')->length(255)->nullable();
                //$table->integer('type')->default(0);
                $table->text('description');
                $table->tinyInteger('enabled')->lenght(1)->default(1);
                $table->string('seo_title')->length(255)->nullable();
                $table->text('seo_description')->nullable();
                $table->string('seo_keywords')->length(255)->nullable();
                $table->string('menu_class')->length(255)->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
