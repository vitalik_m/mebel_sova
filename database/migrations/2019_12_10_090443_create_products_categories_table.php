<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('products_categories')) {
            Schema::create('products_categories', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('title');
                $table->string('slug')->length(255)->nullable();
                //$table->integer('parent')->default(0);
                $table->tinyInteger('enable')->length(1)->unsigned();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_categories');
    }
}
