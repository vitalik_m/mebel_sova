<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->length(255);
            $table->string('title')->length(255);
            $table->string('intro');
            $table->text('content');
            $table->string('enabled')->length(10);
            $table->string('seo_title')->length(255)->nullable();
            $table->text('seo_description')->length(255)->nullable();
            $table->text('seo_keywords')->length(255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
