<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductPropertiesKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('product_property_floats')) {
            Schema::table('product_property_floats', function (Blueprint $table) {
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('cascade');
            });
        }

        if (Schema::hasTable('product_property_bools')) {
            Schema::table('product_property_bools', function (Blueprint $table) {
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('cascade');
            });
        }

        if (Schema::hasTable('product_property_chars')) {
            Schema::table('product_property_chars', function (Blueprint $table) {
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('value_id')->references('id')->on('product_property_char_values');
                $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('cascade');
            });
        }

        if (Schema::hasTable('product_products_property')) {
            Schema::table('product_products_property', function (Blueprint $table) {
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('cascade');
            });
        }

        if (Schema::hasTable('products_category_products_property')) {
            Schema::table('products_category_products_property', function (Blueprint $table) {
                $table->foreign('product_category_id')->references('id')->on('products_categories');
                $table->foreign('product_property_id')->references('id')->on('product_properties');
            });
        }

        if (Schema::hasTable('product_property_char_values')) {
            Schema::table('product_property_char_values', function (Blueprint $table) {
                $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('product_property_floats')) {
            Schema::table('product_property_floats', function (Blueprint $table) {
                $table->dropForeign('product_property_floats_product_property_id_foreign');
            });
        }

        if (Schema::hasTable('product_property_bools')) {
            Schema::table('product_property_bools', function (Blueprint $table) {
                $table->dropForeign('product_property_bools_product_property_id_foreign');
            });
        }

        if (Schema::hasTable('product_property_chars')) {
            Schema::table('product_property_chars', function (Blueprint $table) {
                $table->dropForeign('product_property_chars_value_id_foreign');
                $table->dropForeign('product_property_chars_product_property_id_foreign');
            });
        }

        if (Schema::hasTable('product_products_property')) {
            Schema::table('product_products_property', function (Blueprint $table) {
                $table->dropForeign('product_products_property_product_id_foreign');
                $table->dropForeign('product_products_property_product_property_id_foreign');
            });
        }

        if (Schema::hasTable('products_category_products_property')) {
            Schema::table('products_category_products_property', function (Blueprint $table) {
                $table->dropForeign('products_category_products_property_products_category_id_foreign');
                $table->dropForeign('products_category_products_property_product_property_id_foreign');
            });
        }
    }
}
