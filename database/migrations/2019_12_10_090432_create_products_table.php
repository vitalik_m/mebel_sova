<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->bigIncrements('id');
                
                $table->string('title')->length(255);
                $table->string('main_image')->length(255)->default('');
                $table->string('main_image_alt')->length(255)->default('');
                $table->string('main_image_title')->length(255)->default('');
                $table->text('description');
                //$table->integer('product_category')->default(0);
                $table->string('slug')->length(255)->nullable();
                $table->tinyInteger('enable')->length(1)->unsigned();
                $table->tinyInteger('special_offer')->length(1)->unsigned();
                $table->string('seo_title')->length(255)->nullable();
                $table->text('seo_description')->nullable();
                $table->string('seo_keywords')->length(255)->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
