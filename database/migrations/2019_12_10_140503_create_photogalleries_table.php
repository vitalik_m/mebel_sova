<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotogalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('photogalleries')) {
            Schema::create('photogalleries', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('title')->length(255);
                $table->string('shortcode')->length(255);
                $table->text('images')->nullable();
                $table->text('alts')->nullable();
                $table->text('titles')->nullable();
                $table->tinyInteger('enable')->length(1)->unsigned();
                $table->string('seo_title')->length(255)->nullable();
                $table->text('seo_description')->nullable();
                $table->string('seo_keywords')->length(255)->nullable();
                
                $table->timestamps();
                $table->softDeletes();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photogalleries');
    }
}
