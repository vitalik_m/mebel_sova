<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCategoryProductsPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_category_products_property', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_category_id')->unsigned();
            $table->bigInteger('product_property_id')->unsigned();
            $table->smallInteger('order')->length(2)->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_category_products_property');
    }
}
