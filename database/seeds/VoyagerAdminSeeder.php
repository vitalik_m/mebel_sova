<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VoyagerAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'admin',
            'email'=> 'email@example.com',
            'avatar' => 'users/default.png', // default admin voyager avatar
            'password' => bcrypt('qwerty'),
        ]);
    }
}
