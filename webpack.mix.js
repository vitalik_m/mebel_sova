const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/breadcrumbs.scss', 'public/css');
mix.sass('resources/sass/filters.scss', 'public/css');
// mix.scripts(['resources/js/calendar.js'], 'public/js/calendar.js');
mix.version();