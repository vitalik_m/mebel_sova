<?php


namespace App\Helpers;


use App\Product;
use App\ProductProperty;
use App\ProductPropertyCharValue;
use App\ProductsCategory;

class MigrateCategories
{
    public function __construct()
    {
        $this->products = Product::all();

        $this->createManyToManyRel();
    }

    private function createManyToManyRel()
    {
        foreach ($this->products as $product){
            if($product->products_category_id){
                $product->categories()->attach($product->products_category_id);
            }
        }
    }

}