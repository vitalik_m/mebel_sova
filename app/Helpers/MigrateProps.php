<?php


namespace App\Helpers;


use App\Product;
use App\ProductProperty;
use App\ProductPropertyCharValue;
use App\ProductsCategory;

class MigrateProps
{
    const NAMES = [
        'width' => [ 'title' => 'Ширина', 'slug' => 'width', 'value_type' => 'float'],
        'height' => [ 'title' => 'Высота', 'slug' => 'height', 'value_type' => 'float'],
        'long' => [ 'title' => 'Длина', 'slug' => 'long', 'value_type' => 'float'],
        'sleep_place_width' => [ 'title' => 'Ширина спального места', 'slug' => 'sleep_place_width', 'value_type' => 'float'],
        'sleep_place_height' => [ 'title' => 'Длина спального места', 'slug' => 'sleep_place_length', 'value_type' => 'float'],
        'lifting_mechanism' => [ 'title' => 'Механизм', 'slug' => 'lifting_mechanism','value_type'  => 'char'],
        'furniture_type' => [ 'title' => 'Тип мебели', 'slug' => 'furniture_type','value_type'  => 'char'],
        'color' => [ 'title' => 'Цвет', 'slug' => 'color','value_type' => 'char'],
    ];


    public function __construct()
    {
        $this->createAllProps();
    }

    public function migrate()
    {
        $products = Product::all();

        $products->map(function ($product, $key) {
            if($product->title == 'Асти'){
                $asd = 'asd';
            }

            $names = self::NAMES;
            foreach( $names as $oldSlug => $params){
                if(!$product->$oldSlug) continue;

                $newSlug = self::NAMES[$oldSlug]['slug'];

                $property = ProductProperty::where('slug', '=', $newSlug)->first();
                $product->properties()->syncWithoutDetaching($property->id);

                switch ($params['value_type']) {
                    case 'float':
                        $this->migrateFloats($product, $oldSlug, $newSlug);
                        break;
                    case 'char':
                        $this->migrateChars($product, $oldSlug, $newSlug);
                        break;
//                    case 'bool':
//                        $this->migrateBools($product, $oldSlug);
//                        break;
                }
            }
        });
    }

    private function migrateFloats($product, $oldSlug, $newSlug)
    {
        $productProperty = $product->properties()->get()
            ->keyBy('slug')[$newSlug];

        $productProperty->values()->updateOrCreate([
            'value' => $product->$oldSlug,
            'product_id' => $product->id,
        ]);
    }

    private function migrateChars($product, $oldSlug, $newSlug)
    {
        //setting properties value for current product
        $productProperty = $product->properties()->get()
            ->keyBy('slug')[$newSlug];

        //creating chars value Name
        $value = ProductPropertyCharValue::firstOrCreate(['name' => $product->$oldSlug, 'product_property_id' => $productProperty->id]);

        $res = $productProperty->values()->updateOrCreate([
            'value_id' => $value->id,
            'product_id' => $product->id,
        ]);
    }

    private function createAllProps()
    {
        foreach(self::NAMES as $slug => $prop){
            $prop = ProductProperty::firstOrCreate(
                ['slug' => $prop['slug']], $prop
            );

            $this->attachToAllCategories($prop);
        }
    }

    private function attachToAllCategories($prop)
    {
        foreach(ProductsCategory::all() as $cat){
            $cat->productsProperties()->attach($prop->id);
        }
    }

}