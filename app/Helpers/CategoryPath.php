<?php
function categoryURI(\App\ProductsCategory $category , $fullPath = true)
{
    $uri = $fullPath ? $category->slug: '';

    if($category->parent) {
        $uri = categoryURI($category->parent) . '/' . $uri;
    }

    return $uri;
}

function productPath(\App\Product $product, $category = '')
{
    $categoriesURI = $category ? categoryURI($category) : categoryURI($product->categories[0]);

    $productSlug = $product->slug. '-' .$product->id;

    return route('catalog.product', ['categoriesURI' => $categoriesURI, 'product' => $productSlug]);
}

function categoryPath(\App\ProductsCategory $category)
{
    if($category->parent){
        $categoryURI = categoryURI($category, false);

        $categorySlug = $category->slug . '-' . $category->id;

        $route = route('catalog.subcategory', ['categoriesURI' => $categoryURI, 'category' => $categorySlug]);
    }else{
        $route = route('catalog.category', ['category' => $category->slug]);
    }

    return prepare_url($route);
}

function prepare_url(string $url) {
    $url_array = parse_url($url);
    $url_array['path'] = str_replace('//', '/', $url_array['path']);
    //print_r($url_array);

    $url = '';

    // set protocol, http or https
    if( array_key_exists('scheme', $url_array) ) {
        $url .= $url_array['scheme'];
    } else {
        $url .= 'http';
    }
    $url .= '://';

    // set hostname (domain)
    if( array_key_exists('host', $url_array) ) {
        $url .= $url_array['host'];
    } else {
        $url .= request()->getHttpHost();
    }
    // set port (if exists)
    if( array_key_exists('port', $url_array) ) {
        $url .= ':'.$url_array['port'];
    }

    // set path
    if( array_key_exists('path', $url_array) ) {
        $url .= $url_array['path'];
    }

    // set query string
    if( array_key_exists('query', $url_array) ) {
        $url .= '?'.$url_array['query'];
    }

    return $url;
}