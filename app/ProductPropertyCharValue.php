<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyCharValue extends Model
{
    protected $table = 'product_property_char_values';
    protected $fillable = ['name', 'product_property_id'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function value(){
        return $this->hasMany(ProductPropertyCharValue::class, 'value_id', 'id');
    }

    public function property()
    {
        return $this->belongsTo(ProductProperty::class, 'id', 'product_property_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */


}
