<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\URLs;

class URLsController extends Controller
{
    public function index(Request $request, URLs $urls) {
        return $urls->proxy_request($request);
    }
}
