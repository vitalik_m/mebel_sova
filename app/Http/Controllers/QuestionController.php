<?php

namespace App\Http\Controllers;


use App\Http\Requests\QuestionSentRequest;
use App\Mail\QuestionMail;
use App\Page;
use Illuminate\Support\Facades\Mail;

class QuestionController extends Controller
{
    public function show()
    {
        return view('question.show');
    }

    public function send(QuestionSentRequest $request)
    {
        $form = $request->all();

        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new QuestionMail($form));

        return view('question.show', ['success' => true]);
    }
}
