<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goods;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\GoodsCategory;

class GoodsController extends VoyagerBaseController
{

    protected $categories_array = array();

    public function __construct(GoodsCategory $goods_category) {
        $this->middleware('auth');
        $this->categories_array = $goods_category->goods_categories_hierarchy();
        //dd($this->categories_array);
    }

    
    public function index(Request $request) {
        return view('vendor.voyager.goods', ['goods'=>Goods::all()->toArray()]);
    }

    public function add() {
        return view('vendor.voyager.goods_add', ['categories'=>$this->categories_array]);
    }

    public function new(Request $request, Goods $goods) {
        if($goods->new($request)){
            // ok
        }

        return redirect()->route('voyager.goods.index');
    }

    public function edit_ui(Goods $good) {

        return view('vendor.voyager.goods_edit', ['product'=>$good->toArray(), 'categories'=>$this->categories_array]);
    }

    public function save_changes(Request $request, Goods $good){
        if($good->edit($request, $good)) {
            // ok
        }

        return redirect()->route('voyager.goods.index');
    }

    public function del(Goods $goods){
        $goods->delete();

        return redirect()->route('voyager.goods.index');
    }

}
