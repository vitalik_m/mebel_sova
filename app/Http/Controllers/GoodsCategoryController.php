<?php

namespace App\Http\Controllers;

use App\Goods;
use Illuminate\Http\Request;
use App\GoodsCategory;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController;

class GoodsCategoryController extends VoyagerBaseController
{
    /**
     * Array of categories. This var fill in __construct()
     *
     * @var array
     */
    protected $categories_array = array();

    public function __construct(GoodsCategory $goods_category) {
        $this->categories_array = $goods_category->goods_categories_hierarchy();
        //dd($this->categories_array);
    }

    /**
     * List all categories
     *
     * @param Request $request Laravel Http Request
     * @return void
     */
    public function index(Request $request) {
        return view('vendor.voyager.goods_categories', ['categories'=>$this->categories_array]);
    }

    /**
     * Get template for add new category
     *
     * @return void
     */
    public function add(){
        return view('vendor.voyager.goods_categories_add', ['categories'=>$this->categories_array]);
    }

    /**
     * Save new caegory
     *
     * @param Request $request
     * @param GoodsCategory $goods_category
     * @return void
     */
    public function new(Request $request, GoodsCategory $goods_category){
        if( $goods_category->new($request) ){
            // ok
        }

        return redirect()->route('voyager.goods-categories.index');
    }

    /**
     * Get template for edit category
     *
     * @param GoodsCategory $goods_category
     * @return void
     */
    public function edit_ui(GoodsCategory $goods_category){
        return view('vendor.voyager.goods_category_edit', ['current_category'=>$goods_category->toArray(), 'categories'=>$this->categories_array]);
    }

    /**
     * Get edit category
     *
     * @param GoodsCategory $goods_category
     * @return void
     */
    public function save_changes(Request $request, GoodsCategory $goods_category){
        if( $goods_category->edit($request, $goods_category) ){
            // ok
        }

        return redirect()->route('voyager.goods-categories.index');
    }

    /**
     * Delete category
     *
     * @param GoodsCategory $goods_category
     * @return void
     */
    public function del(GoodsCategory $goods_category){
        if( $goods_category->del($goods_category) ){
            // ok
        }

        return redirect()->route('voyager.goods-categories.index');
    }
}
