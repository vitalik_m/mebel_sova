<?php

namespace App\Http\Controllers;

use App\Helpers\MigrateProps;
use App\ProductsCategory;
use App\Services\FiltersService\FiltersService;
use App\Services\ProductCategoryService;
use Illuminate\Http\Request;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController;
use \App\FiltersField;
use \Illuminate\Pagination\Paginator;
use \Illuminate\Pagination\LengthAwarePaginator;

class ProductCategoryController extends VoyagerBaseController
{
    /**
     * Filter object
     *
     * @var object
     */
    protected $filter = null;
    protected $service;

    /**
     * ProductCategoryController constructor.
     * @param ProductCategoryService $service
     */
    public function __construct(ProductCategoryService $service) {
        $this->filter = new FiltersField();
        $this->service = $service;
    }

    /**
     * List all categories
     *
     * @param Request $request Laravel Http Request
     * @return view
     */
    public function catalog(Request $request) {
        $categories = ProductsCategory::onlyParents()->simplePaginate(setting('site.products_per_page'));

        return view('category.list', ['categories' => $categories]);
    }

    /**
     * List category
     *
     * @param string $category category slug
     * @param Request $request Laravel Http Request
     * @return view|void
     */
    public function view(string $category, Request $request) {
        $category = ProductsCategory::where('slug', $category)->first();
        $children = $category->children()->simplePaginate(setting('site.products_per_page'));

        $products = $this->filter->apply($category->products()->orderBy('id', 'desc')->get());
        $products = new Paginator($products, setting('site.products_per_page'), 1);
        $products->setPath(request()->getPathInfo());
        $products->render();

        $filter = $this->filter->get_fields();

        if($category) {
            return view('category.view', compact(['category', 'children', 'products', 'filter']));
        }
    }

    /**
     * List subcategory
     *
     * @param string $categoryURI 
     * @param string $category category slug
     * @return view|void
     */
    public function subView($categoryURI, $category)
    {
        $category = (new ProductsCategory)->getBySlugID($category);

        $children = $category->children()->simplePaginate(setting('site.products_per_page'));

        $products = (new FiltersService($category->products()))->filter()->orderBy('id', 'desc')->get();
        $page = Paginator::resolveCurrentPage();

        $products = new LengthAwarePaginator(
            $products->forPage($page, setting('site.products_per_page')),
            $products->count(), setting('site.products_per_page'),
            $page,
            ['path' => request()->getPathInfo()]
        );

        $filtersList = FiltersService::getFilters($category);
        $filters = request('filters') ?? null;

        if($category) {
            return view('category.view',
                compact(['category', 'children', 'products', 'filters', 'filtersList']));
        }
    }
}
