<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductPropertyValue;
use App\ProductProperty;
use App\ProductPropertyChar;
use App\ProductPropertyCharValue;
use Illuminate\Http\Request;

class ProductPropertyCharController extends Controller
{

    public function index(ProductProperty $property, ProductPropertyCharValue $values) {
        $values = $values->where('product_property_id', '=', $property->id)->get();

        return view('admin.product_properties.values', compact('property', 'values'));
    }

    public function all(ProductPropertyCharValue $values) {
        $values = ProductPropertyChar::all()->unique('value_id');

        return view('admin.product_properties.values', compact('values'));
    }

    public function store(StoreProductPropertyValue $valueRequest) {
        $valueName = (new ProductPropertyCharValue())->fill([
            'name' => $valueRequest->get('valueName'),
            'product_property_id' => $valueRequest->get('property_id')
        ]);

        $valueName->save();

        return redirect( route('admin.products.property.value.char.names', ['property' => $valueName->product_property_id]))
            ->with(['message' => "Значение добавлено!", 'alert-type' => 'success']);
    }

    public function update(Request $request) {
        $request->validate([
            'name' => 'required|unique:product_property_char_values|max:255',
            'id' => 'required|numeric|exists:product_property_char_values',
        ]);

        $value = ProductPropertyCharValue::find($request->get('id'))->first();
        $value->fill($request->toArray())->update();

        return redirect(route('admin.products.property.value.char.names', ['property' => $value->product_property_id]))
            ->with(['message' => "Значение изменено!", 'alert-type' => 'success']);
    }

    public function delete(Request $request) {
        $request->validate([
            'valueName_id' => 'required|exists:product_property_char_values,id|max:255',
        ]);

        ProductPropertyChar::where('value_id', $request->get('valueName_id'))->delete();
        $valueName = ProductPropertyCharValue::find($request->get('valueName_id'))->first();
        $property_id = $valueName->product_property_id;
        ProductPropertyCharValue::find($request->get('valueName_id'))->delete();

        return redirect(route('admin.products.property.value.char.names', ['property' => $valueName->product_property_id]))
            ->with(['message' => "Значение удалено!", 'alert-type' => 'success']);
    }



}
