<?php

namespace App\Http\Controllers\Admin;


use \TCG\Voyager\Http\Controllers\VoyagerBaseController;
use \App\Page;
use \App\PagesType;
use Illuminate\Http\Request;

class PageController extends VoyagerBaseController
{

    protected $page_type = null;

    public function __construct(){
        $this->page_type = PagesType::all()->toArray();
    }

    public function index(Request $request){
        return view('vendor.voyager.pages', ['pages'=>Page::all()->toArray()]);
    }

    public function add(){
        return view('vendor.voyager.pages_add', ['page_types'=>$this->page_type]);
    }

    public function new(Request $request, Page $page){
        if( $page->new($request) ) {
            return redirect()->route('voyager.pages.index');
        }
    }

    public function edit_ui(Page $page) {
        return view('vendor.voyager.pages_edit', ['page'=>$page->toArray(), 'page_types'=>$this->page_type]);
    }

    public function save_changes(Request $request, Page $page) {
        if( $page->edit($request, $page) ) {
            return redirect()->route('voyager.pages.index');
        }
    }

    public function del(Page $page) {
        if( $page->delete() ){
            return redirect()->route('voyager.pages.index');
        }
    }

    public function show_by_type(Request $request, $type='page') {
        $page = new Page();
        return $page->show_by_type($request, $type);
    }
}
