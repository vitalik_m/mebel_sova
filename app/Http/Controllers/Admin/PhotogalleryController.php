<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhotogalleryDestroyRequest;
use App\Http\Requests\PhotogalleryStoreRequest;
use App\Http\Requests\PhotogalleryUpdateRequest;
use Illuminate\Http\Request;
use \App\Photogallery;

class PhotogalleryController extends Controller
{
    public function index()
    {
        $galleries = Photogallery::all();

        return view('admin.photogallery.index', compact('galleries'));
    }

    public function show(Request $request, Photogallery $photogallery) {
        return redirect(route('admin.photogallery.edit', ['photogallery' => $photogallery->id]));
    }

    public function create()
    {
        return view('admin.photogallery.create');
    }

    public function store(PhotogalleryStoreRequest $request, Photogallery $photogallery)
    {
        $photogallery->add($request);

        return redirect()->route('admin.photogallery.index')
            ->with(
                [
                    'message' => 'Фотогаллерея <a href="'. route('admin.photogallery.edit', $photogallery) .'">'. $photogallery->title.' </a> создана!',
                    'alert-type' => 'success'
                ]
            );
    }

    public function edit(Request $request, Photogallery $photogallery)
    {
        return view('admin.photogallery.edit', ['gallery' => $photogallery]);
    }

    public function update(PhotogalleryUpdateRequest $request, Photogallery $photogallery)
    {
        $photogallery->save_changes($request, $photogallery);

        return redirect()->route('admin.photogallery.index')
            ->with([
                'message' => 'Фотогаллерея <a href="'. route('admin.photogallery.edit', $photogallery) .'">'. $photogallery->title.' </a> обновлена!',
                'alert-type' => 'success'
            ]);
    }

    public function destroy(Photogallery $photogallery) {
        $photogallery->del($photogallery);

        return redirect()->route('admin.photogallery.index')
            ->with(['message' => "Фотогаллерея удалена!", 'alert-type' => 'success']);
    }

    public function destroyItem(Photogallery $photogallery, int $id ) {

        if($photogallery->delItem($id)){
            return response()->json([
                'status' => 'ok',
                'msg' => $id+1 . '-й елемент фотогаллереи удален']
            );
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Ошибка елемент не был уделен'],
        403);
    }
}
