<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductsCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function updateProductProperties(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:products_categories|max:255',
            'properties.*' => 'required|exists:product_properties,id|numeric',
        ]);


        $arr = [];
        if($request->get('properties')) {
            foreach ($request->get('properties') as $i => $id) {
                $arr[$id] = ['order' => $i + 1];
            }
        }

        $category = ProductsCategory::findOrFail($request->get('id'));

        $category->productsProperties()->sync($arr);

        return back()->with(['message' => "Фильтры обновлены!", 'alert-type' => 'success']);
    }

}
