<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductPropertyChar;
use App\ProductPropertyFloat;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function updateProperties(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:products|max:255',
            'float.*.property_id' => 'exists:product_properties,id|numeric',
            'float.*.value' => 'nullable|numeric',
            'char.*.property_id' => 'required|exists:product_properties,id|numeric',
            'char.*.value_id' => 'nullable|numeric|exists:product_property_char_values,id',
        ]);

        $product = Product::findOrFail($request->get('id'));

        $floats = $request->get('float');
        if($floats) {
            foreach ($floats as $value) {
                $propertyValue = ProductPropertyFloat::firstOrNew(['product_id' => $product->id, 'product_property_id' => $value['property_id']]);
                if ($value['value']) {
                    $propertyValue->fill(['product_id' => $product->id, 'product_property_id' => $value['property_id'], 'value' => (float)$value['value']]);
                    $propertyValue->save();
                    $product->properties()->attach($value['property_id']);
                } else {
                    $product->properties()->detach($value['property_id']);
                    $propertyValue->delete();
                }
            }
        }

        $chars = $request->get('char');
        if($chars){
            foreach ($chars as $value){
                $propertyValue = ProductPropertyChar::firstOrNew(['product_id' => $product->id, 'product_property_id' => $value['property_id']]);
                if($value['value_id']){
                    $propertyValue->fill(['product_id' => $product->id, 'product_property_id' => $value['property_id'], 'value_id' => $value['value_id']]);
                    $propertyValue->save();
                    $product->properties()->attach($value['property_id']);
                }else{;
                    $product->properties()->detach($value['property_id']);
                    $propertyValue->delete();
                }
            }
        }

        return back()->with(['message' => "Свойства обновлены!", 'alert-type' => 'success']);
    }
}
