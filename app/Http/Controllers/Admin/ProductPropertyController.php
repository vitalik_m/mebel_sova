<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductProperty;
use App\ProductProperty;
use App\ProductPropertyChar;
use App\ProductPropertyCharValue;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductPropertyController extends Controller
{
    public function index(ProductProperty $properties) {
        $properties = $properties->all();
        $valuesNames = ProductPropertyCharValue::all();

        return view('admin.product_properties.index', compact('properties', 'valuesNames'));
    }

    public function store(StoreProductProperty $request) {
        $property = (new ProductProperty)->fill([
            'title' => $request->get('title'),
            'slug' => Str::slug($request->get('title'), '-'),
            'unit' => $request->get('unit'),
            'value_type' => $request->get('value_type'),
        ]);

        $property->save();

        return redirect( route('admin.products.properties'))
            ->with(['message' => "Свойство добавлено!", 'alert-type' => 'success']);
    }

    public function update(Request $request) {
        $request->validate([
            'id' => 'required|exists:product_properties|max:255',
            'title' => 'required|unique:product_properties,title,'. $request->get('id') .'|max:255|string',
            'unit' => 'nullable|max:255|string',
        ]);

        $property = ProductProperty::findOrFail($request->get('id'))->fill([
            'title' => $request->get('title'),
            'slug' => Str::slug($request->get('title'), '-'),
            'unit' => $request->get('unit'),
        ]);

        $property->update();

        return redirect( route('admin.products.properties'))
            ->with(['message' => "Свойство обновлено!", 'alert-type' => 'success']);
    }

    public function delete(Request $request) {
        $request->validate([
            'id' => 'required|exists:product_properties|max:255',
        ]);

        $property = ProductProperty::find($request->get('id'));

        $property->products()->detach();
        $property->productsCategories()->detach();
        $property->values()->delete();

        $property->delete();

        return redirect(route('admin.products.properties'))
            ->with(['message' => "Свойство удалено!", 'alert-type' => 'success']);
    }
}
