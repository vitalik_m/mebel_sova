<?php

namespace App\Http\Controllers;


use App\Product;
use App\ProductsCategory;
use Illuminate\Support\Facades\Storage;

class PageFrontController extends Controller
{
    public function home()
    {
        $specialOffers = Product::specials()->get();

        return view('pages.home', ['specialOffers' => $specialOffers]);
    }

    public function action()
    {
        $specialOffers = Product::specials()->get();

        return view('pages.home', ['specialOffers' => $specialOffers]);
    }
}
