<?php

namespace App\Http\Controllers;


use App\Article;

class ArticleController extends Controller
{
    public function list() {
        $articles = Article::enabled()->get();

        return view('articles.list', ['articles'=> $articles]);
    }

    public function view(string $slug) {
        $article = (new Article)->getBySlug($slug)->firstOrFail();

        return view('articles.view', ['article'=> $article]);
    }
}
