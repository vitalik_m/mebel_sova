<?php

namespace App\Http\Controllers;



use App\Page;
use App\Product;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(Page $page)
    {
        $specialOffers = Product::specials()->get();
        $page = $page->get_by_slug('home')->firstOrFail();

        return view('pages.home', compact('specialOffers', 'page'));
    }

    public function actions(Page $page)
    {
        $specialOffers = Product::specials()->get();
        $page = $page->get_by_slug('actions')->first();

        return view('pages.actions', compact('specialOffers', 'page'));
    }

    public function contacts(Page $page)
    {
        $page = $page->get_by_slug('contacts')->firstOrFail();

        return view('pages.contacts', compact('page'));
    }

    public function static(Request $request, Page $page)
    {
        $page = $page->get_by_slug($request->path())->firstOrFail();

        return view('pages.static', ['page' => $page]);
    }

    public function articles()
    {
        $specialOffers = Product::specials()->get();

        return view('pages.home', ['specialOffers' => $specialOffers]);
    }

    public function search(Request $request)
    {
        $subject = '%'.$request->get('word').'%';
        $result = Product::where('description', 'like', $subject)->orWhere('title', 'like', $subject)
            ->enabled()->has('categories')->get();

        return view('pages.search', ['result' => $result]);
    }
}
