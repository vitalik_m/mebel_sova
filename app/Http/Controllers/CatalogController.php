<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;

class CatalogController extends Controller
{
    public function show(Request $request) {
        $catalog = new Catalog();
        if( (bool)($items = $catalog->show($request)) ) {
            return view('catalog', ['items'=>$items->toArray()]);
        }

        return abort(404);
    }
}
