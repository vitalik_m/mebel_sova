<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Sitemap;

class SitemapController extends Controller
{
    public function index(Request $request) {
        $sitemap = new Sitemap();
        $sitemap->generate();
    }
}
