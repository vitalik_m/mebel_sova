<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductProperty;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function view(Request $request, $categoriesURI, $product)
    {
         $product = (new Product)->getBySlugID($product);

        if($product) {
            return view('product.view', ['product' => $product]);
        }
    }

}
