<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['slug', 'title', 'intro', 'content', 'enabled', 'seo_title', 'seo_description', 'seo_keywords' ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getBySlug(string $slug) {
        return $this->where('slug', $slug);
    }

    public function scopeEnabled()
    {
        return self::where ('enabled', 'on')->orWhere('enabled', 1);
    }
}
