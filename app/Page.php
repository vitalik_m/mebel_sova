<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Page extends Model
{

    public function show_by_type(Request $request, $type) {
        $url = parse_url($request->getRequestUri());
        $parsed_url_path = explode('/', trim($url['path'], '/'));
        return Page::where('slug', array_pop($parsed_url_path))->first();
    }

    public function get_by_slug(string $slug) {
        return $this->where('slug', $slug);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function type(){
        return $this->belongsTo(PagesType::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeEnabled($query)
    {
        return $query->where ('enabled', 1);
    }
}
