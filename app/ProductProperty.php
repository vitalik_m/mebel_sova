<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
    protected $table = 'product_properties';
    protected $fillable = [ 'title', 'slug', 'unit', 'value_type'];

    protected static $valuesClasses = [
        'bool' => ProductPropertyBool::class,
        'float' => ProductPropertyFloat::class,
        'char' => ProductPropertyChar::class
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_products_property', 'product_property_id', 'product_id');
    }

    public function values()
    {
        if($this->value_type == 'special') return null;

        return $this->hasMany(self::$valuesClasses[$this->value_type], 'product_property_id', 'id');
    }

    public function value()
    {
        if(!$this->pivot || !$this->pivot->product_id || $this->value_type == 'special') return null;

        return $this->hasOne(self::$valuesClasses[$this->value_type], 'product_property_id', 'id')
            ->where('product_id', '=', $this->pivot->product_id);
    }

    public function charsValues()
    {
        if($this->value_type != 'char') return null;

        return $this->hasMany(ProductPropertyCharValue::class, 'product_property_id', 'id');
    }

    public function productsCategories(){
        return $this->belongsToMany(ProductsCategory::class, 'products_category_products_property', 'product_property_id', 'product_category_id');
    }
}
