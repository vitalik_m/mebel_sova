<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyChar extends Model
{
    protected $table = 'product_property_chars';
    protected $fillable = ['value_id', 'product_id', 'product_property_id'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function property()
    {
        return $this->belongsTo(ProductProperty::class, 'product_property_id', 'id');
    }

    public function valueName()
    {
        return $this->belongsTo(ProductPropertyCharValue::class, 'value_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getValueAttribute()
    {
        return $this->valueName->name;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

//    public function setValueAttribute($value)
//    {
//        $this->value['first_name'] = strtolower($value);
//    }
}
