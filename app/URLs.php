<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
//use Route;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PhotogalleryController;

class URLs extends Model
{
    public function proxy_request(Request $request) {
        if( preg_match('/^\/\bcatalog\b/i', $request->getRequestUri()) ) {
            //echo('is catalog');
            $catalog = new CatalogController();
            return $catalog->show($request);
            //return CatalogController::show($request);
        } else {
            //echo('is page, articles or other');
            $page = new PageController();
            $gallery = new PhotogalleryController();
            if( (bool)$page->show_by_type($request, 'page') ) {
                // page
                return view('page', ['content'=>$page->show_by_type($request, 'page')->toArray()]);
            } elseif ( (bool)$gallery->show($request) ) {
                // gallery
                return view('page', ['content'=>$gallery->show($request)->toArray()]);
            }
        }

        // page not found. generate 404
        return abort(404);
    }
}
