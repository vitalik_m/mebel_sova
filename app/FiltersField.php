<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FiltersFieldsType;
use App\Product;
use \Request;
use \App\ProductsCategory;
use \Illuminate\Database\Eloquent\Collection;


class FiltersField extends Model
{
    /**
     * Return HTML filter
     *
     * @return string
     */
    public function get_fields() {
        $product_category = new ProductsCategory();
        $category = explode('/', str_ireplace('//', '/', request()->getPathInfo()));
        $current_category = array_pop($category);
        if( !empty($category) ) {
            $parent_category = array_pop($category);
        } else {
            $parent_category = null;
        }
        $product_category = $product_category->where('slug', $current_category)->first();
        $products = $product_category->withCategory($current_category);

        $fields = $this->all();
        if( empty($products) ) {
            // if not found products from current category - get all products
            $products = new Product();
            $products = $products->all()->toArray();// array of products
        }
        
        $filter_html = '';

        $filter_html .= '<form action="'.Request::url().'" id="filter" class="filter">';
        foreach ($fields as $key => $field) {
            if( empty($field->for_field) ) {
                continue;
            }
            
            if( ( $field->for_field == 'furniture_type' ) ) {
                if ( ( $product_category->id != 11 ) and ( $product_category->parent_category != 11 ) ) {
                    continue;
                }
            }

            switch ($field->type) {
                case 'text' :
                    // text field
                    $filter_html .= $this->text($field->slug, $field->name);
                break;
                case 'checkbox' :
                    // checkbox
                    $filter_html .= $this->checkbox($field->slug, $field->name);
                break;
                case 'between' :
                    // range
                    $products_of_search_fields = array_column($products, $field->for_field, $field->for_field);
                    $products_of_search_fields = array_filter($products_of_search_fields);
                    if( !empty($products_of_search_fields) ) {
                        $min = min($products_of_search_fields);
                        $max = max($products_of_search_fields);
                    } else {
                        $min = 0;
                        $max = 0;
                    }
                    $current_min = (int) request()->{$field->slug."_start"};
                    $current_max = (int) request()->{$field->slug."_stop"};
                    $filter_html .= $this->beetween($field->slug, $field->name, $min, $max, $current_min, $current_max);
                break;
                case 'dropdown' :
                    // dropdown (select box)
                    $products_of_search_fields = array_column($products, $field->for_field, $field->for_field);

                    // explode parameters
                    foreach($products_of_search_fields as $key => $search_field) {
                        //$search_field = htmlentities($search_field, ENT_QUOTES|ENT_DISALLOWED);
                        $products_of_search_fields = array_merge($products_of_search_fields, explode(',', $search_field));
                        unset($products_of_search_fields[$key]);
                    }
                    unset($key, $search_field);

                    // rebuild array
                    foreach($products_of_search_fields as $key => $search_field) {
                        $filter_val = ucfirst(trim($search_field));
                        $products_of_search_fields[$filter_val] = $filter_val;
                        unset($products_of_search_fields[$key]);
                    }

                    $products_of_search_fields = array_filter($products_of_search_fields);
                    $products_of_search_fields['default'] = 'Показать все';
                    $products_of_search_fields = array_unique($products_of_search_fields);
                    $products_of_search_fields = array_reverse($products_of_search_fields);

                    $filter_html .= $this->dropdown($field->slug, $field->name, $products_of_search_fields);
                break;
            }
        }
        $filter_html .= '<button type="submit">Фильтровать</button>';
        $filter_html .= '</form>';

        return $filter_html;
    }

    /**
     * Apply flilter for product list
     *
     * @param \Illuminate\Database\Eloquent\Collection $products products from this category
     * @return void
     */
    public function apply(Collection $products) {
        $fields = $this->all();
        foreach ($fields as $key => $field) {
            if( is_array($products) ) {
                $products = collect($products);
            }

            $field_name = $field->slug;
            if( ($field->type == 'between') and (!empty(Request::get($field_name.'_start'))) and (!empty(Request::get($field_name.'_stop'))) ) {
                $field_ranges = array_column($products->toArray(), $field->for_field);
                $field_ranges = array_filter($field_ranges);
                if( !empty($field_ranges) ) {
                    if( (Request::get($field_name.'_start') === min($field_ranges)) and 
                        (Request::get($field_name.'_stop') === max($field_ranges))
                    ) {
                        // selected all range, skip this filter, for display all ranges
                        continue;
                    }

                    $products = $products->whereBetween($field->for_field, [Request::get($field_name.'_start'), Request::get($field_name.'_stop')]);
                }
            }

            if( empty(Request::get($field_name)) ) {
                // no paramether value, skip this filter for display all ranges
                continue;
            }
            
            $decoded_fieldvalue = urldecode(Request::get($field_name));
            $category = ProductsCategory::where('title', $decoded_fieldvalue)->first();
            $products = $products->where($field->for_field, $decoded_fieldvalue);
            if( $products->isNotEmpty() ) {
                //dd($products);
                //$products = $products->get();
            } elseif( !is_null($category) ) {
                $products = $category->products()->where($field->for_field, $decoded_fieldvalue);
                if( !empty($products) ) {
                    $products = $products->get();
                }
                
            }
        }

        return $products;
    }

    /**
     * Create dropdown
     *
     * @param string $name Name of option. This name locate in "name" tag attribute
     * @param string $title Title of option.
     * @param array $options Options array. Array should has structure ['value'=>'title']
     * @return string
     */
    protected function dropdown(string $name, string $title, array $options) {
        $result = '';

        $result .= '<div>';
        $result .= '<label>';
        $result .= '<h6>'.$title.'</h6>';
        $result .= '<select name="'.$name.'">';
        foreach ($options as $value => $title) {
            if($value === 'default'){
                $value = '';
            }
            if(urldecode(Request::get($name)) === $value) {
                $select = ' selected="selected"';
            } else {
                $select = '';
            }
            $result .= '<option value="'.urlencode($value).'"'.$select.'>'.$title.'</option>';
        }
        $result .= '</select>';
        $result .= '</label>';
        $result .= '</div>';

        return $result;
    }

    /**
     * Create text field
     *
     * @param string $name Name of option. This name locate in "name" tag attribute
     * @param string $title Title of option.
     * @return string
     */
    protected function text(string $name, string $title) {
        $result = '';
        $value = '';
        if(Request::get($name)) {
            $value = ' value="'.Request::get($name).'"';
        }

        $result .= '<div>';
        $result .= '<label>';
        $result .= '<h6>'.$title.'</h6>';
        $result .= '<input type="text" name="'.$name.'"'.$value.' />';
        $result .= '</label>';
        $result .= '</div>';

        return $result;
    }

    /**
     * Create checkbox
     *
     * @param string $name Name of option. This name locate in "name" tag attribute
     * @param string $title Title of option.
     * @return string
     */
    protected function checkbox(string $name, string $title) {
        $result = '';

        $result .= '<div>';
        $result .= '<label>';
        $result .= '<h6>'.$title.'</h6>';
        $result .= '<input type="text" name="'.$name.'" />';
        $result .= '</label>';
        $result .= '</div>';

        return $result;
    }

    /**
     * Create field for select range
     *
     * @param string $name Name of option. This name locate in "name" tag attribute
     * @param string $title Title of option.
     * @return string
     */
    protected function beetween(string $name, string $title, int $min, int $max, int $current_min=0, int $current_max=0) {
        $result ='';

        $result .= '<div>';
        $result .= '<label>';
        $result .= '<h6>'.$title.'</h6>';
        $result .= '<div id="slider-range"></div>';
        $result .= '<script>
                        document.addEventListener(\'DOMContentLoaded\', function() {
                            var jquery_ui_object = null;
                            $(function() {
                                $( "#slider-range" ).slider({
                                range: true,
                                min: '.$min.',
                                max: '.$max.',
                                values: [ '.( (bool)$current_min ? $current_min : $min ).', '.( (bool)$current_max ? $current_max : $max ).' ],
                                slide: function( event, ui ) {
                                        jquery_ui_object = ui;
                                        //$( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                                        $( "#'.$name.'_start" ).val(ui.values[ 0 ]);
                                        $( "#'.$name.'_stop" ).val(ui.values[ 1 ]);
                                    }
                                });
                            });
                            $("#'.$name.'_start").change(function() {
                                $("#slider-range").slider(\'values\',0,$(this).val());
                            });
                            $("#'.$name.'_stop").change(function() {
                                $("#slider-range").slider(\'values\',1,$(this).val());
                            });
                        });
                    </script>
        ';
        $result .= '<input name="'.$name.'_start" id="'.$name.'_start" type="number" class="price_range" value="'.( (bool)$current_min ? $current_min : $min ).'" />';
        $result .= '<input name="'.$name.'_stop" id="'.$name.'_stop" type="number" class="price_range" value="'.( (bool)$current_max ? $current_max : $max ).'" />';
        $result .= '</label>';
        $result .= '</div>';

        return $result;
    }
}
