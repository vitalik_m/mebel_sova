<?php

namespace App;

use App\Services\PhotogalleryService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class Photogallery extends Model
{
    static public function getStoragePath()
    {
        return Storage::disk('public')->getAdapter()->getPathPrefix();
    }

    public function getFilePath()
    {
        return 'photogalleries'.DIRECTORY_SEPARATOR.$this->shortcode.DIRECTORY_SEPARATOR;
    }

    static public function makeNewFileName($name)
    {
        return base64_encode(request()->file($name)->getClientOriginalName()) .'-'.
            pathinfo(request()->file($name)->getClientOriginalName(), PATHINFO_FILENAME) .'.'.
            request()->file($name)->extension();
    }

    public function save_changes(Request $request, Photogallery $photogalery) {
        $photogalleryService = new PhotogalleryService($photogalery);

        $exist_images = $photogalery->images;
        $exist_alts = $photogalery->alts;
        $exist_titles = $photogalery->titles;
        $exist_display = $photogalery->display_as;
        $fields_for_sub_product = $photogalery->fields_for_subproduct;

        $flag_file_changed = false;

        // update fields
        if( $request->has('gallery_upload_0_alt') ) {
            $i = 0;
            while( $request->has('gallery_upload_'.$i.'_alt') ) {

                $exist_images[$i] = $request->get('gallery_upload_'.$i.'_old_img');
                $exist_alts[$i] = $request->get('gallery_upload_'.$i.'_alt');
                $exist_titles[$i] = $request->get('gallery_upload_'.$i.'_title');
                $exist_display[$i] = $request->get('gallery_upload_'.$i.'_display');

                $fields_for_sub_product[$i]['title'] = $request->get('title_for_'.$i.'_sub_0');
                $fields_for_sub_product[$i]['key'] = $request->get('key_for_'.$i.'_sub_0');
                $fields_for_sub_product[$i]['value'] = $request->get('value_for_'.$i.'_sub_0');

                $i++;
            }
        }

        // change gallery slug detected
        extract($photogalleryService->changeSlugIfNeed($exist_images, $flag_file_changed));

        if( $request->files->count() ) {
            foreach($request->files as $name => $file) {
                $itemNum = (integer) str_replace('gallery_upload_', '', $name);

                $request->file($name)->storeAs($this->getFilePath(), $this->makeNewFileName($name));

                $exist_images[$itemNum] = $this->getFilePath().$this->makeNewFileName($name);
            }
            $flag_file_changed = true;
        }

        extract($photogalleryService->deleteIfNeed($exist_images, $exist_alts, $exist_titles, $exist_display, $fields_for_sub_product, $flag_file_changed));

        $photogalery->enabled = $request->get('enable') && $request->get('enable') == 'on' ? true : false;

        // if files changed
        if( $flag_file_changed ){
            $photogalery->images = $exist_images;
            $photogalery->alts = $exist_alts;
            $photogalery->titles = $exist_titles;
            $photogalery->display_as = $exist_display;
            $photogalery->fields_for_subproduct = $fields_for_sub_product;

            $photogalleryService->sortItems();
        }

        $photogalery->save();
    }

    public function add(Request $request) {
        $this->title = $request->get('title');
        $this->shortcode = $request->get('shortcode');

        // upload all files and save alts and titles
        $field_num = 0;
        $files_to_save = array();
        $titles_to_save = array();
        $alts_to_save = array();
        $display = array();
        $fields_for_sub_product = array();

        if( $request->files->count() ) {
            foreach($request->files as $name => $file) {
                $itemNum = (integer) str_replace('gallery_upload_', '', $name);

                $request->file($name)->storeAs($this->getFilePath(), $this->makeNewFileName($name));

//                $files_to_save[$itemNum] = $this->getFilePath().$this->makeNewFileName($name);
                $files_to_save[] = $this->getFilePath().$this->makeNewFileName($name);                

                $titles_to_save[] = $request->get('gallery_upload_'.$field_num.'_title');
                $alts_to_save[] = $request->get('gallery_upload_'.$field_num.'_alt');
                $display[] = $request->get('gallery_upload_'.$field_num.'_display');
                $fields_for_sub_product_new = array();
                $fields_for_sub_product_new['title'] = $request->get('title_for_'.$field_num.'_sub_0');
                $fields_for_sub_product_new['key'] = $request->get('key_for_'.$field_num.'_sub_0');
                $fields_for_sub_product_new['value'] = $request->get('value_for_'.$field_num.'_sub_0');
    
                $fields_for_sub_product[] = $fields_for_sub_product_new;

                $field_num++;
            }
        }

        $this->enabled = $request->get('enable') && $request->get('enable') == 'off' ? true : false;
        $this->images = $files_to_save;
        $this->display_as = $display;
        $this->alts = $alts_to_save;
        $this->titles = $titles_to_save;
        $this->fields_for_subproduct = $fields_for_sub_product;

        $this->save();
    }

    public function del(Photogallery $photogalery) {
        $path_to_dir = $this->getStoragePath().'photogalleries'.DIRECTORY_SEPARATOR.$photogalery->shortcode;
        $deleted_flag = false;
        if( file_exists($path_to_dir) ) {
            $directory_iterator = new \RecursiveDirectoryIterator($path_to_dir, \FilesystemIterator::SKIP_DOTS);
            $directory_iterator->rewind();
            while($directory_iterator->valid()) {
                unlink($path_to_dir.DIRECTORY_SEPARATOR.$directory_iterator->getSubPathName());
                $directory_iterator->next();
            }
            if( rmdir($path_to_dir) ) {
                $photogalery->delete();
                $deleted_flag = true;
            }
        }

        if( !$deleted_flag ) {
            $photogalery->delete();
        }
    }

    /**
     * @param int $deletingItemId
     * @return bool
     */
    public function delItem(int $deletingItemId) {
        Storage::delete($this->images[$deletingItemId]);

        $this->images = array_diff_key($this->images, [$deletingItemId => '']);
        $this->alts = array_diff_key($this->alts, [$deletingItemId => '']);
        $this->titles = array_diff_key($this->titles, [$deletingItemId => '']);
        $this->display_as = array_diff_key($this->display_as, [$deletingItemId => '']);
        $this->fields_for_subproduct = array_diff_key($this->fields_for_subproduct, [$deletingItemId => '']);

        return $this->save();
    }

    public function prepare_to_get(Photogallery $photogalery) {
        $images = array();

        foreach($photogalery->images as $image) {
            $images[] = Storage::disk('public')->url($image);
        }

        $photogalery['images'] = $images;

        return $photogalery;
    }

    public function show(Request $request) {
        $url = parse_url($request->getRequestUri());
        $parsed_url_path = explode('/', trim($url['path'], '/'));
        $photogalery = Photogallery::where('shortcode', array_shift($parsed_url_path))->first();
        if( (bool)$photogalery ) {
            return $this->prepare_to_get($photogalery);
        }
        return null;
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = serialize($value);
    }

    public function setAltsAttribute($value)
    {
        $this->attributes['alts'] = serialize($value);
    }

    public function setDisplayAsAttribute($value)
    {
        $this->attributes['display_as'] = serialize($value);
    }

    public function setTitlesAttribute($value)
    {
        $this->attributes['titles'] = serialize($value);
    }

    public function setFieldsForSubproductAttribute($value)
    {
        $this->attributes['fields_for_subproduct'] = serialize($value);
    }


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getImagesAttribute($value)
    {
        if( is_array($value) ) {
            return $value;
        }
        return unserialize($value);
    }

    public function getAltsAttribute($value)
    {
        if( is_array($value) ) {
            return $value;
        }
        return unserialize($value);
    }

    public function getDisplayAsAttribute($value)
    {
        if( is_array($value) ) {
            return $value;
        }
        return unserialize($value);
    }

    public function getTitlesAttribute($value)
    {
        if( is_array($value) ) {
            return $value;
        }
        return unserialize($value);
    }

    public function getFieldsForSubproductAttribute($value)
    {
        if( is_array($value) ) {
            return $value;
        }
        return unserialize($value);
    }
}
