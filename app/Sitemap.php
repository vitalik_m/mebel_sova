<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Page;
use App\Photogallery;
use App\ProductsCategory;
use App\Product;

class Sitemap extends Model
{

    protected $catalog_prefix = 'catalog/';

    protected $product_prefix = 'product/';

    protected $urls_for_sitemap = array();

    public $sitemap = "";

    public function generate() {
        $this->urls_for_sitemap = array_merge_recursive(Page::where('enabled', true)->get()->toArray(), $this->urls_for_sitemap);
        $categories = ProductsCategory::where('enabled', true)->get();
        $sub_categories_array = array();
        $products_array = array();
        foreach( $categories as &$category ) {
            // Get subcategory
            $sub_categories = $category->children()->get();
            if( $sub_categories->isNotEmpty() ) {
                foreach($sub_categories as $sub_category) {
                    $sub_category = $this->prepare_subcategories($category, $sub_category);

                    $sub_categories_array[] = $sub_category->toArray();
                    $products_array[] = $this->prepare_products($sub_category);
                    $products_array = array_filter($products_array);
                }
                //dump($sub_category->products());
            }
        }
        $this->urls_for_sitemap = array_merge_recursive($this->urls_for_sitemap, $sub_categories_array, ...$products_array);

        header('Content-Type: application/xml');
        echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        echo("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
        foreach ($this->urls_for_sitemap as $item) {
        echo("\t<url>\n");
        echo("\t\t<loc>".env('APP_URL').($item['slug'] ?? $item['shortcode'])."</loc>\n");
        echo("\t\t<lastmod>".$item['updated_at']."</lastmod>\n");
        echo("\t\t<changefreq>monthly</changefreq>\n");
        echo("\t\t<priority>0.5</priority>\n");
        echo("\t</url>\n");
        }
        echo("</urlset>\n");
        exit;
    }


    protected function prepare_subcategories(ProductsCategory $category, ProductsCategory $sub_category) {
        //$sub_category = $sub_category->toArray();
        $sub_category->slug = $this->catalog_prefix.$category->slug.'/'.$sub_category->slug;
        return $sub_category;
    }

    protected function prepare_products(ProductsCategory $category){
        $products = $category->products()->get();
        foreach($products as &$product) {
            $product->slug = $category->slug.'/'.$this->product_prefix.$product->slug;
        }
        return $products->toArray();
    }
}
