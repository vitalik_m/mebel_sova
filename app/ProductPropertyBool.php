<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyBool extends Model
{
    protected $table = 'product_property_bools';
    protected $fillable = ['value', 'product_id', 'product_property_id'];


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */


}
