<?php

namespace App\Providers;

use App\ProductsCategory;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Voyager;
use Illuminate\Auth\Access\Gate;
use TCG\Voyager\Models\Menu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        /*
        $voyager = new Voyager();
        $voyager->useModel('Goods', \App\Goods::class);
        */
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(2000); // fix migration error for mysql 5.6 // old value 191
        //View::share('menu', ProductsCategory::onlyParents()->get());
        //View::share('fMenu', menu('main', '_json'));

        $menu = Menu::display('MainMenu','_json');
        View::share('menu', $menu);

    }
}
