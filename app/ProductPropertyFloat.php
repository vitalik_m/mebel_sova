<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPropertyFloat extends Model
{
    protected $table = 'product_property_floats';
    protected $fillable = ['value', 'product_id', 'product_property_id'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function property(){
        return $this->belongsTo(ProductProperty::class, 'id', 'product_property_id');
    }


}
