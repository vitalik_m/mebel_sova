<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Product;
use App\ProductsCategory;


class Catalog extends Model
{
    public function show(Request $request) {
        
        $url = parse_url($request->getRequestUri());
        $parsed_url_path = explode('/', trim($url['path'], '/'));
        $path_count = count($parsed_url_path);
        if ($path_count == 3) {
            // is product

            $category = ProductsCategory::where('slug', array_slice($parsed_url_path, 1, 1))->with('products')->firstOrFail();
            $products_in_category = $category->products()->get();
            $product = $products_in_category->where('slug', array_pop($parsed_url_path))->first();
            //$product = Product::where('slug', '=', array_pop($parsed_url_path))->get();
            return !empty($product) ? $product : null;
        } elseif ($path_count == 2) {
            // is category
            //ProductsCategory::find(9)->products()->get() // test ... OK
            $category = ProductsCategory::where('slug', array_slice($parsed_url_path, 1, 1))->with('products')->firstOrFail();
            return $category->products()->get();
        } elseif ($path_count == 1) {
            // is catalog page
            return null;
        }
//        return Product::all();
    }
}
