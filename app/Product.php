<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
//    protected $attributes = [
//        'enabledProperties' => 0,
//        'collections' => 0,
//    ];

    public function getBySlugID(string $product)
    {
        $productAttrs = explode('-', $product);

        return self::find(end($productAttrs));
    }

    public function getEnabledProperties()
    {
        $properties = collect();

        foreach( $this->categories as $category){
            $properties = $category->productsProperties->keyBy('id');
        }

        return $properties;
    }

    public function getCollections()
    {
        $collections = [];

        if($this->categories && $this->categories->count() > 1){
            foreach( $this->categories as $category){
                if($category->is_collection){
                    $collections[] = $category;
                }
            }
        }

        return $collections;
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeSpecials($query)
    {
        return $query->where ('special_offer', 1);
    }

    public function scopeEnabled($query)
    {
        return $query->where ('enabled', 1);
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function properties(){
        return $this->belongsToMany(
            ProductProperty::class,
            'product_products_property',
            'product_id',
            'product_property_id');
    }

    public function photo(){
        return $this->hasOne(Photogallery::class, 'id', 'product_gallery');
    }

    public function categories(){
        return $this->belongsToMany(
            ProductsCategory::class,
            'product_products_category',
            'product_id',
            'products_category_id');
    }
}
