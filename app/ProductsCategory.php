<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsCategory extends Model
{
    
    protected $primaryKey = 'id';

    /**
     * Get products from category
     *
     * @param string $category Category or subcategory slug
     * @return array
     */
    public function withCategory(string $category){
        $current_category = $this->where('slug', $category)->first();
        if( !empty($current_category) ) {
            return $current_category->products()->get()->toArray();
        } else {
            return array();
        }
    }

    public function getBySlugID(string $category)
    {
        $productAttrs = explode('-', $category);

        return self::find(end($productAttrs));
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

//    public function products(){
//        return $this->belongsToMany(Product::class);
//    }

    public function products(){
        return $this->belongsToMany(
            Product::class,
            'product_products_category',
            'products_category_id',
            'product_id');
    }

    public function productsProperties(){
        return $this->belongsToMany(
            ProductProperty::class,
            'products_category_products_property',
            'product_category_id',
            'product_property_id')->withPivot('order');
    }

    public function parent(){
        return $this->hasOne(ProductsCategory::class, 'id', 'parent_category');
    }

    public function children(){
        return $this->hasMany(ProductsCategory::class, 'parent_category', 'id');

    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeOnlyParents(){
        return self::whereNull('parent_category');
    }
    
}
