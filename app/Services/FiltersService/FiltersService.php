<?php


namespace App\Services\FiltersService;

use App\ProductsCategory;

class FiltersService
{
    use FiltersMethods;

    public $products;
    public $filters;

    public function __construct( $products)
    {
        $this->products = $products;
        $this->filters = request('filters');
    }

    /**
     * @param $entity
     * @return mixed
     */
    public static function getFilters(ProductsCategory $category)
    {
        return $properties = $category->productsProperties()->orderBy('order')->get()->keyBy('slug');
    }

    /**
     * @return mixed
     */
    public function filter()
    {
        if(empty($this->filters)) return $this->products;

        foreach($this->filters as $type => $properties){
            $this->$type($properties);
        }

        return $this->products;
    }
}