<?php


namespace App\Services\FiltersService;

 trait FiltersMethods
{
     private function special($properties)
     {
         foreach($properties as $param => $value){
             switch ($param){
                 case 'cost':
                     $this->products->whereBetween($param, $value);
                     break;
             }
         }

//         $productIds = $this->products->get('products.id');
     }

    //TODO refactor
     private function float($properties)
     {
         foreach($properties as $id => $value) {
             $this->products->join('product_property_floats AS ppf_'.$id, function ($join) use($id, $value) {
                 $join->on('products.id', '=', 'ppf_'.$id.'.product_id');
                 $join->where('ppf_'.$id.'.product_property_id', '=', $id)
                     ->whereBetween('ppf_'.$id.'.value', $value);
             });
         }
     }

     //TODO refactor
     private function char($properties)
     {
         foreach($properties as $propSlug => $value_id) {
             if(!$value_id) continue;

             $this->products->join('product_property_chars AS ppc_'.$propSlug, function ($join) use($propSlug, $value_id) {
                 $join->on('products.id', '=', 'ppc_' . $propSlug . '.product_id');
                 $join/*->where('ppc_' . $propSlug . '.product_property_id', '=', $id)*/
                     ->where('ppc_' . $propSlug . '.value_id', '=', $value_id);
             });
         }
     }

     //TODO refactor
     private function bool($properties)
     {
         foreach($properties as $id => $value) {
             $this->products->join('product_property_bools AS ppb_'.$id, function ($join) use($id) {
                 $join->on('products.id', '=', 'ppb_'.$id.'.product_id');
                 $join->where('ppb_'.$id.'.product_property_id', '=', $id)
                     ->where('ppb_'.$id.'.value', '=', '1');
             });
         }
     }
}