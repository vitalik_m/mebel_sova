<?php
/**
 * User: Vitaliy Mezhebitskiy
 * Date: 30.04.2020
 */

namespace App\Services;


use Illuminate\Support\Facades\Storage;

class PhotogalleryService
{
    public $photogallery;
    public $storage_path;

    public function __construct($photogallery)
    {
        $this->photogallery = $photogallery;
        $this->storage_path = $photogallery->getStoragePath();
    }

    public function deleteIfNeed($exist_images, $exist_alts, $exist_titles, $exist_display, $fields_for_sub_product, $flag_file_changed)
    {
        $deleted_images = explode(',', trim(request()->get('deleted_images'), ','));

        if( !empty($deleted_images) ) {

            foreach ($deleted_images as $id) {
                if ( array_key_exists($id, $exist_images) ) {
                    unset($exist_images[$id]);
                }

                if ( array_key_exists($id, $exist_alts) ) {
                    unset($exist_alts[$id]);
                }

                if ( array_key_exists($id, $exist_titles) ) {
                    unset($exist_titles[$id]);
                }

                if ( array_key_exists($id, $exist_display) ) {
                    unset($exist_display[$id]);
                }

                if ( array_key_exists($id, $fields_for_sub_product) ) {
                    unset($fields_for_sub_product[$id]);
                }
            }

            $flag_file_changed = true;
        }

        return compact('exist_images','exist_alts', 'exist_titles', 'exist_display', 'fields_for_sub_product', 'flag_file_changed');
    }

    public function changeSlugIfNeed($exist_images, $flag_file_changed)
    {
        if( ($this->photogallery->title !== request()->get('title'))
            or ($this->photogallery->shortcode !== request()->get('shortcode')) ){

            @rename($this->storage_path.'photogalleries'.DIRECTORY_SEPARATOR.$this->photogallery->shortcode,
                $this->storage_path.'photogalleries'.DIRECTORY_SEPARATOR.request()->get('shortcode'));

            $images = array();
            foreach((array)$exist_images as $image) {
                $images[] = str_replace($this->photogallery->shortcode, request()->get('shortcode'), $image);
            }

            $exist_images = $images;

            $this->photogallery->title = request()->get('title');
            $this->photogallery->shortcode = request()->get('shortcode');

            $flag_file_changed = true;
        }

        return compact('exist_images', 'flag_file_changed');
    }

    public function sortItems()
    {
        $exist_images = $this->photogallery->images;
        $exist_alts = $this->photogallery->alts;
        $exist_titles = $this->photogallery->titles;
        $exist_display = $this->photogallery->display_as;
        $fields_for_sub_product = $this->photogallery->fields_for_subproduct;

        $shouldIndex = 0;
        foreach ($exist_images as $index => $item){
            if($index != $shouldIndex){
                $exist_images[$shouldIndex] = $exist_images[$index];
                $exist_alts[$shouldIndex] =  $exist_alts[$index];
                $exist_titles[$shouldIndex] =  $exist_titles[$index];
                $exist_display[$shouldIndex] =  $exist_display[$index];
                $fields_for_sub_product[$shouldIndex] =  $fields_for_sub_product[$index];

                unset($exist_images[$index]);
                unset($exist_alts[$index]);
                unset($exist_titles[$index]);
                unset($exist_display[$index]);
                unset($fields_for_sub_product[$index]);
            }

            $shouldIndex++;
        }

        $this->photogallery->images = $exist_images;
        $this->photogallery->alts = $exist_alts;
        $this->photogallery->titles = $exist_titles;
        $this->photogallery->display_as = $exist_display;
        $this->photogallery->fields_for_subproduct = $fields_for_sub_product;
    }
}